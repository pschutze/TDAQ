#!/bin/bash

ROOT=$(cd `dirname $0`; pwd -P)

if [ -z "$TDAQ_PARTITION" ]; then
  source setup_RCDTDAQ.sh 
fi
ohp -p ${TDAQ_PARTITION} -c $ROOT/Configs/isotdaq_ohp.xml
