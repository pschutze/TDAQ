#!/bin/bash

ROOT=$(cd `dirname $0`; pwd -P)

if [ -z "$TDAQ_PARTITION" ]; then
  source setup_RCDTDAQ.sh
fi

trt -p $TDAQ_PARTITION -c $ROOT/Configs/trt_test_beam.cfg
