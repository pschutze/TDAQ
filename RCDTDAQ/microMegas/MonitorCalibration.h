#ifndef __MONITOR_CALIBRATION__ 
#define __MONITOR_CALIBRATION__ 

#include "MonitorBase.h"

#include <vector>
#include <list>
#include <map>

#include <TH1.h>
#include "TGraph.h"
#include <TFile.h>

class CUDPData;
class CEventDecoder;
class CEventVectors;

 class MonitorCalibration: public MonitorBase {
// class MonitorCalibration {
  public:
    MonitorCalibration();
    MonitorCalibration(std::string name, CConfiguration* config, std::string chamberName, int stripNumber,
                   std::string runNumber, bool publish);
    virtual ~MonitorCalibration();

    void SetDataContainerPtr(std::list <CUDPData*>* dc) {m_datacontainerPtr =  dc; }
    void SetEventContainerPtr(std::list <CMMEvent*>* ec) {m_eventcontainerPtr =  ec; }

    void SetHistProvider(OHRootProvider* prov) { m_histProvider = prov; }

    void SetEventDecoder(CEventDecoder* evd) {m_decoder = evd; }
    void SetEventVectors(CEventVectors* evv) {m_eventVectors =  evv; }

    int buildEventVectors(std::vector<unsigned int>& rawEvent);
    void pedestalVectors();

    virtual bool Process(std::vector<unsigned int>&  ev);
    virtual void Print(std::vector<unsigned int>& ev);
    virtual void WriteToFile();
    virtual void PrepareRootFile(std::string runNumberStr);
    virtual void PublishHists();

  private:
    std::vector <TH1F*> m_hStripQmax;

    OHRootProvider* m_histProvider;

    bool m_firstEvent;

    int m_lowStrip, m_highStrip;	// for ONE chamber
    std::map<int,int> m_stripToChannel;

// pedestal VECTORS
    std::vector<float> m_pedestal_mean;
    std::vector<float> m_pedestal_stdev;

/******** USER DEFINITION *****************/
    float m_sigma_cut_factor = 3.0;
/******************************************/

    std::string m_chamberName;
    int m_stripNumber;
    TFile * m_outputFile;
    std::string m_MonitorName;

    std::list <CUDPData*>*        m_datacontainerPtr;
    std::list <CMMEvent*>*        m_eventcontainerPtr;

    CConfiguration*      m_config;
    CEventDecoder*       m_decoder;
    CEventVectors*       m_eventVectors;
};

  
#endif
