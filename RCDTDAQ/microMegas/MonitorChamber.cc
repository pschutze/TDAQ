#include "CUDPData.h"
#include "CConfiguration.h"
#include "CDetChamber.h"
#include "CEventDecoder.h"
#include "CEventVectors.h"

#include <oh/OHRootProvider.h>

#include "MonitorChamber.h"

using namespace std;

int buildEventVectors(std::vector<unsigned int>& rawEvent);
void buildFrameContainer(vector<unsigned int>& rawEvent, std::list <CUDPData*>* m_datacontainer);
std::pair<unsigned,unsigned> stripRange(CConfiguration* config, std::string chamberName);

MonitorChamber::MonitorChamber(std::string name, CConfiguration* config, std::string chamberName, int stripNumber,
                               std::string runNumberStr, bool publish):
                               m_computeCorrelations(false),
                               m_timeLow(0),
                               m_timeHigh(1e9)
{
  m_MonitorName = name;
  #warning "WIP: stripNumber is ignored in this build - bane"
  m_config = config;

  m_firstEvent = true;

  pedestalVectors();
 
  if(m_config->defined_chamber_names().size() > 1){
     m_computeCorrelations=true;
  }

// BEFORE booking histograms
  if (!publish) {
    PrepareRootFile(runNumberStr);
  }

// defining histograms
// this could also be done in the 'firstEvent' part of Process IF event parameters are needed to define the histos
  string lastChmName="";
  int chNum=-1;
  for (auto chamberName: m_config->defined_chamber_names()){
    m_nameMap[chamberName]=++chNum;
    TH1* addHist(0);

    if (chNum>0){
      addHist = new TH2I((std::string("corr_")+lastChmName+"_"+chamberName).c_str(),
                         (std::string("corr_")+lastChmName+"_"+chamberName).c_str(),
                         1024,0,1024,1024,0,1024);
      addHist->SetDrawOption("COLZ");
      m_correlation.push_back((TH2I*)addHist);
    }

    addHist = new TH1F((chamberName + "_qmax").c_str(),
                        (chamberName + " Qmax all channels").c_str(),
                        200, 0, 2.0);
    addHist->GetXaxis()->SetTitle("max strip charge");
    addHist->SetCanExtend(TH1::kAllAxes);
    m_maxQ.push_back((TH1F*)addHist);

    addHist = new TH1I((chamberName + "_qmax_cluster").c_str(),
                        (chamberName + " Qmax cluster charge").c_str(),
                        500, 0, 10000);
    addHist->GetXaxis()->SetTitle("Qmax cluster charge");
    m_maxQ_event_cluster.push_back((TH1I*)addHist);

    addHist = new TH1I((chamberName + "_qmaxEvent").c_str(),
                        (chamberName + " Event Qmax").c_str(),
                        50, 0, 2000);
    addHist->GetXaxis()->SetTitle("max strip charge of event");
//    addHist->SetCanExtend(TH1::kAllAxes);
    m_maxQ_event.push_back((TH1I*)addHist);

    m_sRange = stripRange(m_config, chamberName);
    int numStrips = m_sRange.second - m_sRange.first +1;
std::cout << " chamber name = " << chamberName << " test " << " numStrips = " <<
             numStrips << " first = " << m_sRange.first << " last = " << m_sRange.second << std::endl;

    addHist = new TH1I((chamberName + "_MaxQev_strip").c_str(),
                       (chamberName + " Strip number of Event Qmax").c_str(),
                       numStrips,m_sRange.first,m_sRange.second);
    addHist->GetXaxis()->SetTitle("strip number");
    m_maxQ_event_strip.push_back((TH1I*)addHist);

    // # channels = # strips
    addHist = new TH1I((chamberName + "_MaxQev_channel").c_str(),
                       (chamberName + " Channel number of Event Qmax").c_str(),
                       numStrips,0,2047);   // TOO hardwired
    addHist->GetXaxis()->SetTitle("channel number");
    m_maxQ_event_channel.push_back((TH1I*)addHist);
  
    addHist = new TH1I((chamberName + "_MaxQev_time").c_str(),
                       (chamberName + " Time bin  of Event Qmax").c_str(),
                       30,0,30);
    // m_maxQ_event_time->SetCanExtend(TH1::kAllAxes);
    addHist->GetXaxis()->SetTitle("timebin");
    m_maxQ_event_time.push_back((TH1I*)addHist);

    m_multiplicity.push_back(new TH1I((chamberName + "_multiplicity").c_str(),
                                   (chamberName + " Multiplicity at Qmax Event").c_str(),
                                   20,0,20));
 
// histogram only makes sense for ZS, otherwise will result in overflows ...
    addHist = new TH1I((chamberName + "_no_channels").c_str(),
                       (chamberName + " # channels").c_str(),
                       100,0,100);
    addHist->GetXaxis()->SetTitle("number of channels");
    m_noChannels.push_back((TH1I*)addHist);

    lastChmName = chamberName;
  }

// common for chambers
  m_number_emptyEvents = new TH1I("number_empty_events", " number of empty events",
                                   10, 0, 10);
  m_number_emptyEvents->GetXaxis()->SetTitle(" # empty events");

}

MonitorChamber::~MonitorChamber()
{
}

bool
MonitorChamber::Process(vector<unsigned int>& ev)
{

  // all APV fragments empty - cannot build vectors
  int retBuild = buildEventVectors(ev);
// std::cout << " retBuild = " << retBuild << std::endl;

  if( retBuild != CEventVectors::RETURN_OK) {
  std::cout << " retBuild before return = " << retBuild << std::endl;
    m_number_emptyEvents->Fill(1);
    return true;
  }

  unsigned int apv_evt =                        m_eventVectors->get_apv_evt();
  std::vector <unsigned int> apv_fec =          m_eventVectors->get_apv_fec();
  std::vector <unsigned int> apv_id  =          m_eventVectors->get_apv_id();
  std::vector <unsigned int> apv_ch  =          m_eventVectors->get_apv_ch();
  std::vector <std::string>  mm_id =            m_eventVectors->get_mm_id();
  std::vector <unsigned int> mm_readout =       m_eventVectors->get_mm_readout();
  std::vector <unsigned int> mm_strip    =      m_eventVectors->get_mm_strip();
  std::vector <std::vector <short> > apv_q =    m_eventVectors->get_apv_q();
  std::vector <short> apv_qmax =                m_eventVectors->get_apv_qmax();
  std::vector <short> apv_tbqmax =              m_eventVectors->get_apv_tbqmax();

  auto chamberNames = m_config->defined_chamber_names();
 // for (int i=0; i<chamberNames.size(); i++){
 //   string chamberName = chamberNames[i];

  if (m_firstEvent) {

    int apv_ptr = 9 ;  // RODHDRSIZE
    std::string ap(1,(char)((ev[apv_ptr+3] & 0x00ff0000)>>16));
    if (ap == "Z") {
      m_zsEnable = 1;
      std::cout << " ZS mode " << std::endl;
    }

    std::cout << " apv_evt = " << apv_evt << std::endl;

    std::cout << " size of apv_fec = " << apv_fec.size()
              << " fecNo[0] = " << apv_fec[0]  << std::endl;

    std::cout << " size of apv_id = " << apv_id.size()
              << " id[0] = " << apv_id[0]
              << " id[apv_id.size()-1] = " << apv_id[apv_id.size()-1]
              << std::endl;

    std::cout << " size of apv_ch = " << apv_ch.size()
              << " ch[0] = " << apv_ch[0]  << std::endl;

    std::cout << " size of mm_id = " << mm_id.size()
              << " mm_id[0] = " << mm_id[0]
              << " mm_id[mm_id.size()-1] = " << mm_id[mm_id.size()-1]  << std::endl;

    std::cout << " size of mm_readout = " << mm_readout.size()
              << " mm_readout[0] = " << mm_readout[0]
              << " mm_readout[mm_readout.size()-1] = " << mm_readout[mm_readout.size()-1]  << std::endl;

    std::cout << " size of mm_strip = " << mm_strip.size()
                << " mm_strip[0] = " << mm_strip[0]
                << " mm_strip[mm_strip.size()-1] = " << mm_strip[mm_strip.size()-1]  << std::endl;

    std::cout << " size of apv_qmax = " << apv_qmax.size()
              << std::endl;

   // ONLY makes sense for no ZS. Else the stripToChannel map is event dependent
   for (int ii=0; ii<chamberNames.size(); ii++) {
   // create strip to channel MAP for ONE chamber
     for (int jj = 0; jj<mm_strip.size(); jj++) {  // jj = apvChannel
//       std::cout << " jj = " << jj << " name from data = " <<  mm_id[jj]
//                 << " chamber Name = " << chamberNames[ii] << std::endl;
       if (mm_id[jj] == chamberNames[ii]) {
         int strip = mm_strip[jj];
//         std::cout << " apvChannel = "  << jj << " strip = " << strip << std::endl;
         m_stripToChannelChamber[strip] =  jj;
       }
     }
     m_stripToChannel.push_back(m_stripToChannelChamber);
     m_stripToChannelChamber.clear();
   }

    m_firstEvent = false;
  }

  map<unsigned,int> maxQ_event;   	// max Q of event
  map<unsigned,int> maxStrip_event;	// strip of maxQ_event
  map<unsigned,int> maxApvCh_event;	// APV channel of maxQ_event
  
  for (unsigned i=0; i<chamberNames.size(); i++){
    maxQ_event[i] = -1;
    maxStrip_event[i] = -1;
    maxApvCh_event[i] = -1;
  }
  
  int apvChannel = 0;

// the max calculation assumes pedestal corrected data !!
  for (auto row = apv_q.begin(); row != apv_q.end(); ++row, ++apvChannel)
  {
    string currentChamber = mm_id[apvChannel];
    unsigned currentId = m_nameMap[currentChamber];

    std::vector<int16_t>::iterator imax = max_element(row->begin(), row->end());	// Q max over timebins in one channel
//    std::cout << " apvChannel = " << apvChannel << " strip = " << mm_strip[apvChannel]
//              << " Q max = " << *imax << " chamber name = " << mm_id[apvChannel] << std::endl;
    int tbQmax = std::distance(row->begin(),  imax);
    //std::cout << " tbQmax = " << tbQmax << std::endl;
    
    if (tbQmax < m_timeLow || tbQmax > m_timeHigh){
      continue;
    }
    int maxQ = *imax;
    //if (mm_id[apvChannel] == m_chamberName) {
    m_maxQ[currentId]->Fill(maxQ);
    //}

      if(maxQ > maxQ_event[currentId]) {
        maxQ_event[currentId] = maxQ;
        maxStrip_event[currentId] = mm_strip[apvChannel];
        maxApvCh_event[currentId] = apvChannel;
      }
    
  }
 
  for (int currentId=0; currentId<m_nameMap.size(); currentId++){
    if ( maxQ_event[currentId] == -1 ){
      continue;
    }

    if (m_computeCorrelations && currentId>0){
      m_correlation[currentId-1]->Fill(maxStrip_event[currentId-1], maxStrip_event[currentId]);
    }
  
    m_maxQ_event[currentId]->Fill(maxQ_event[currentId]);
    m_maxQ_event_strip[currentId]->Fill(maxStrip_event[currentId]);
    m_maxQ_event_channel[currentId]->Fill(maxApvCh_event[currentId]);

    std::vector<int16_t>::iterator tmax = max_element(apv_q[maxApvCh_event[currentId]].begin(),
                                                      apv_q[maxApvCh_event[currentId]].end());

    int tbQmax_event = std::distance(apv_q[maxApvCh_event[currentId]].begin(),tmax);
  
//  std::cout << " maxApvCh_event = " << maxApvCh_event <<  " tmax = " << *tmax
//            << " tbQmax_event = " << tbQmax_event << std::endl;
    m_maxQ_event_time[currentId]->Fill(tbQmax_event);

// define sigma_cut_factor in the .h file 
// multiplicity: start with Qmax Event at a given channel,timebin.
// Find number of hits in +- 10 strips around this point.

    if (m_zsEnable == 0) {   // no zero suppression (but pedestal subtraction)
      if (m_pedestal_stdev.size()!=0){
        int multiplicity = 0;
        int clusterCharge = 0;
        int kmin = -10;
        int kmax =  10;
        int numStrips = m_sRange.second - m_sRange.first +1;
//      std::cout << " # strips = " << numStrips << std::endl;

        for (int k=kmin; k<kmax; k++) {
          int strip = maxStrip_event[currentId]+k;
          if (strip > 0 && strip <= numStrips) {
            int chan = m_stripToChannel[currentId][strip];
//            std::cout << " currentId = " << currentId << " strip = " << strip
//                      << " chan = " << chan << std::endl;
            int qq = apv_q[chan][tbQmax_event];
//            std::cout << " tbQmax_event = " << tbQmax_event << " apv_q = "
//                      << apv_q[chan][tbQmax_event] << std::endl;
//            std::cout << " strip number = " << mm_strip[chan] << std::endl;
            float pedstd = m_pedestal_stdev[chan];
//            std::cout << " pedestal STD = " << pedstd << std::endl;
            if (qq > (pedstd*m_sigma_cut_factor)) {   // mean pedesal already subtracted
              multiplicity++;
              clusterCharge += qq;
            }
          }
        }
        //std::cout << " multiplicity = " << multiplicity << std::endl;
        m_multiplicity[currentId]->Fill(multiplicity);
        const int maxCharge = m_maxQ_event_cluster[currentId]->GetXaxis()->GetXmax();
        if (clusterCharge > maxCharge){
          clusterCharge = maxCharge; // last bin is overflow
        }
        m_maxQ_event_cluster[currentId]->Fill(clusterCharge);
      }
    }

    if (m_zsEnable == 1) { 
      int multiplicity = 0;
      int noChannels = 0;
      int qq;
      int clusterCharge = 0;
      std::vector <unsigned int> stripChamber;
      std::vector <int> stripCharge;
      // loop over ALL channels/chambers but ONLY use for THIS chamber
       for (int jj = 0; jj<mm_strip.size(); jj++) {  // jj = apvChannel index
//         std::cout << " ZS, jj = " << jj << " name from data = " <<  mm_id[jj]
//                   << " ZS, chamber Name = " << chamberNames[currentId] << std::endl;
         if (mm_id[jj] == chamberNames[currentId]) {
           int strip = mm_strip[jj];
           qq = apv_q[jj][tbQmax_event];
           std::cout << " ZS, apvChannel = "  << jj << " chamber name = "
                     << chamberNames[currentId] << " qq = " << qq
                     <<  " strip = " << strip << std::endl;
           stripChamber.push_back(strip);
           stripCharge.push_back(qq);

           noChannels++;   // for this chamber
         }
       }
       // 'find' will always find, by definition of  maxStrip_event[currentId]
       std::vector<unsigned int>::iterator im = find(stripChamber.begin(),stripChamber.end(), maxStrip_event[currentId]);
       std::cout << " ZS, size of stripChamber = " << stripChamber.size() << " max strip = " << *im << std::endl;
       int jj = 1;
       int index;
       for ( std::vector<unsigned int>::iterator is = im; is != stripChamber.begin();) { // begin could be OK ??
        is--;
        if (*is == *im - jj) {
          multiplicity++;
          index = std::distance(stripChamber.begin(),is);
          clusterCharge += apv_q[index][tbQmax_event];
          std::cout << " ZS: index = " << index << " charge = " << apv_q[index][tbQmax_event] << std::endl;
        }
        else {
          break;  // for
        }
        std::cout << " ZS: multiplicity = " << multiplicity << std::endl;
        jj++;
       }
       jj = 1;
       for ( std::vector<unsigned int>::iterator is = im; is != stripChamber.end();) {
        is++;
        if (*is == *im + jj) {
          multiplicity++;
          index = std::distance(stripChamber.begin(),is);
          clusterCharge += apv_q[index][tbQmax_event];
          std::cout << " ZS: index = " << index << " charge = " << apv_q[index][tbQmax_event] << std::endl;
        }
        else {
          break;  // for
        }
        std::cout << " ZS: multiplicity = " << std::dec << multiplicity << std::endl;
        jj++;
       }
       multiplicity++;  // the max itself
       index = std::distance(stripChamber.begin(),im);
       clusterCharge += apv_q[index][tbQmax_event];
       std::cout << " ZS: index = " << index << " cluster charge = " << clusterCharge << std::endl;
       m_multiplicity[currentId]->Fill(multiplicity);
       m_maxQ_event_cluster[currentId]->Fill(clusterCharge);
 
       m_noChannels[currentId]->Fill(noChannels);

    }
  }   // one chamber

 return true;
}

void
MonitorChamber::Print(std::vector<unsigned int>& ev)
{
}

void
MonitorChamber::WriteToFile() {

  std::cout << " MonitorChamber::WriteToFile " << std::endl;

  std::string fileName = m_outputFile->GetPath();
  m_outputFile->Write();
  m_outputFile->Close();
  std::cout << " MonitorChamber::WriteToFile " << " Closed file " << fileName << std::endl;
}

void
MonitorChamber::PublishHists() {
  for (int i=0; i<m_config->defined_chamber_names().size(); i++){
    m_histProvider->publish(*m_maxQ[i],m_maxQ[i]->GetName());
    m_histProvider->publish(*m_maxQ_event[i],m_maxQ_event[i]->GetName());
    m_histProvider->publish(*m_maxQ_event_strip[i],m_maxQ_event_strip[i]->GetName());
    m_histProvider->publish(*m_maxQ_event_channel[i],m_maxQ_event_channel[i]->GetName());
    m_histProvider->publish(*m_maxQ_event_time[i],m_maxQ_event_time[i]->GetName());
    m_histProvider->publish(*m_maxQ_event_cluster[i],m_maxQ_event_cluster[i]->GetName());
    m_histProvider->publish(*m_multiplicity[i],m_multiplicity[i]->GetName());
    if (m_zsEnable == 1) {
      m_histProvider->publish(*m_noChannels[i],m_noChannels[i]->GetName());
    }
    if (i<m_config->defined_chamber_names().size()-1){
      m_histProvider->publish(*m_correlation[i], m_correlation[i]->GetName());
    }
  }
  m_histProvider->publish(*m_number_emptyEvents,m_number_emptyEvents->GetName());
}

int MonitorChamber::buildEventVectors(std::vector<unsigned int>& rawEvent) {

  int err = 0;

  buildFrameContainer(rawEvent,m_datacontainerPtr );
//  if (err != 0 ) {
//    std::cout << " buildEventVectors " << " err = " << err << std::endl;
//    return err;
//  }

  std::cout << " buildEventVectors " << " size of data container = " << (*m_datacontainerPtr).size() << std::endl;

  err = m_decoder->buildEventContainer();

//  std::cout << " ProcessEvents " << " size of event container = " << (*m_eventcontainerPtr).size() << std::endl;

  err = m_eventVectors->buildVectors();

  std::cout << " ProcessEvents" << " after buildVectors(), err = " << err << std::endl;

  return err;
}

void MonitorChamber::pedestalVectors() {

  const std::map<int, double>* pedestals_mean =  m_config->pedestal_mean_map();
  const std::map<int, double>* pedestals_stdev =  m_config->pedestal_stdev_map();
  std::cout << " MonitorChamber::pedestalVectors " << " size of pedestals_mean = " << pedestals_mean->size() << std::endl;

// vectorize the maps, indexed by 'sequential' channel number as apv vectors
  int ii = 0;
//  std::cout << " Means " << std::endl;
  for(auto it = pedestals_mean->begin(); it != pedestals_mean->end(); ++it, ii++ ) {
//    std::cout << it->first << " " << it->second << "\n";
    m_pedestal_mean.push_back(it->second);
  }

/********************************************************
  for (int jj = 0; jj<m_pedestal_mean.size(); jj++) {
  std::cout << " MonitorChamber::pedestalVectors " <<  " jj = " << jj
            << " m_pedestal_mean[jj] = "  << m_pedestal_mean[jj] << std::endl;
  }
*********************************************************/

  ii = 0;
//  std::cout << " Stdev s " << std::endl;
  for(auto it = pedestals_stdev->begin(); it != pedestals_stdev->end(); ++it, ii++ ) {
//    std::cout << it->first << " " << it->second << "\n";
    m_pedestal_stdev.push_back(it->second);
  }

/*************************************************
  for (int jj = 0; jj<m_pedestal_stdev.size(); jj++) {
  std::cout << " MonitorChamber::pedestalVectors " <<  " jj = " << jj
            << " m_pedestal_stdev[jj] = "  << m_pedestal_stdev[jj] << std::endl;
  }
****************************************************/

}


void MonitorChamber::PrepareRootFile(std::string runNumberStr) {

// Store histograms in root file mirroring the OH hierarchy
// construct the ROOT file name here in 'user' space
// BEFORE booking histograms

  TDirectory * histogramming_dir = 0;
  TDirectory * monitor_dir = 0;

  const char * histogramming = "Histogramming";
  const char * monitor = "Chamber_monitor";
  string output_name;

  output_name.append("/afs/cern.ch/work/d/daquser/public/bl4smonitor/");
  output_name.append(runNumberStr);
  output_name.append(monitor);
  output_name.append(".root");
  printf(" MonitorChamber::prepareRootFile, Output file name: %s\n", output_name.c_str());
  m_outputFile = new TFile(output_name.c_str(), "RECREATE");
// give Tfile a structure
  histogramming_dir = m_outputFile->mkdir(histogramming);
  histogramming_dir->cd();
  monitor_dir = histogramming_dir->mkdir(monitor);
  monitor_dir->cd();	// current directory

}
