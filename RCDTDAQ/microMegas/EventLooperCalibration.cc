#include <boost/format.hpp>
#include "EventLooperCalibration.h"

EventLooperCalibration::EventLooperCalibration():
  m_histProvider(NULL),
  m_publishHists(false),
  m_publishCycleTime(30),
  m_dataFile("")
{
  m_lastPublicationTime = time(NULL);
}

template <typename EventReader, typename Source>
void 
EventLooperCalibration::ProcessEvents(Source src, int nEvents, bool multirun)
{
  EventReader evReader(src,multirun);
  cout << " EventLooperCalibration::ProcessEvents " << " run number = " << evReader.GetRunNumber() << endl;
  cout << " EventLooperCalibration::ProcessEvents " << " m_publishHists = " << m_publishHists << endl;

  std::vector<std::unique_ptr<MonitorBase>> vMonitors;
  std::unique_ptr<MonitorCalibration> mCalibration(new MonitorCalibration("mCalibration", m_config, m_chamberName, m_stripNumber, 
                                                              to_string(evReader.GetRunNumber()), m_publishHists));

  // transmit to Monitor 
  mCalibration->SetDataContainerPtr(m_datacontainerPtr);
  mCalibration->SetEventContainerPtr(m_eventcontainerPtr);

  mCalibration->SetEventDecoder(m_decoder);
  mCalibration->SetEventVectors(m_eventVectors);

  mCalibration->SetHistProvider(m_histProvider);

  vMonitors.push_back(std::move(mCalibration));
 
  cout << " # monitors = " << vMonitors.size() << endl;

  cout << "Number of events: " << nEvents << endl;
  int eventCount = 0;
  while (eventCount < nEvents || nEvents == -1) {
    cout << "\n # Event No.: " << eventCount << endl;
    vector<unsigned int> rawEvent = evReader.GetNextRawEvent();
    // std::cout << "EventLooperCalibration::ProcessEvents " << " rawEvent size = " << rawEvent.size() << std::endl;
    int evSize = rawEvent.size();
    if(rawEvent.size() == 0)    // End Of File
      break;
    // skip events with status ne 0
    int status = rawEvent[evSize-4];  // REVIEW with new FileEventReader
    if (status !=0) {
      std::cout << " status = " << status << " event size = " << evSize << " eventCount = " << eventCount << std::endl;
      continue;
    }

//    RCDRawEvent ev = RCDRawEvent(rawEvent);aa NOT relevant to microMegas!

//    for(const unsigned int& it : rawEvent) {
//      cout << boost::format("%08x") % it << " ";
//    }

    for(auto & monitor : vMonitors) {
      monitor->Process(rawEvent);
//      monitor->Print(ev);
    }

    if(m_publishHists && IsTimeToPublish())
    {
      for(auto & monitor : vMonitors)
      monitor->PublishHists();
    }
    ++eventCount;
  }

  if (!m_publishHists) {
    for(auto & monitor : vMonitors) {
      monitor->WriteToFile();
      std::cout << "EventLooperCalibration::ProcessEvents " << " ROOT file written " << std::endl;
    }
  }
}

template void EventLooperCalibration::ProcessEvents<DAQEventReader, IPCPartition>(IPCPartition, int, bool);
template void EventLooperCalibration::ProcessEvents<FileEventReader, std::string>(std::string, int, bool);

bool EventLooperCalibration::IsTimeToPublish()
{
  time_t currentTime = time(NULL);
  if( static_cast<long int>(currentTime-m_lastPublicationTime) < m_publishCycleTime)
    return false;
  m_lastPublicationTime = time(NULL);
  return true;
}
