/**************************************************/
/*  mmCalibration Monitor                         */
/*                                                */
/*  2017/06/02                                    */
/*  author:  J.Petersen                           */
/**************************************************/

#include <unistd.h>
#include <signal.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/stat.h>
#include <iostream>
#include <sstream>
#include <string>
#include <fstream>

#include <cmdl/cmdargs.h>
#include <ipc/core.h>
#include <oh/OHRootProvider.h>

#include "DAQEventReader.h"
#include "FileEventReader.h"
#include "EventLooperCalibration.h"

// MM classes
#include "CConfiguration.h"
#include "CEventDecoder.h"
#include "CEventVectors.h"
#include "CUDPData.h"
#include "CEvent.h"

#include "mmCalibration.h"
#include "argArray.h"

template<typename EventReader, typename Source>
void ProcessEvents(Source, int nEvents, bool multirun);

int prepareArg(char argvConfig[argRows][argColumns], char* pArg[], const char rfn[], const CmdArgStr pedFile,
               const CmdArgStr configFile, const int cModeDis);
void init_mm( int argcConfig, char* pArg []);
void delete_mm();
void term_handler(int);

int 
main(int argc, char** argv)
{
  CmdArgStr partition_name ('p', "partition", "partition-name", "partition to work in." );
  CmdArgInt events         ('e', "events", "events-number", "number of events to retrieve (default 1)\n"
                                                         "-1 means work forever" );
  CmdArgStr chamberName    ('c', "chamberName", "chamber-name", "microMegsa chamber name");
  CmdArgInt stripNumber    ('d', "stripNumber", "strip-number", "microMegsa chamber strip number");
  CmdArgStr runType        ('r', "runType", "run-type", "run type: physics/pedestal");

  CmdArgStr datafilename   ('f', "datafilename", "data-file-name", "Data file name, read events from the file instead of online stream");
  CmdArgSet multiFileRun   ('m', "multirun", "Run over all files of a run (offline) or continuously over run boundaries (online)");

  CmdArgStr pedestalFile   ('s', "pedestalFile", "file-name", "MM pedestal file name");
  CmdArgStr configFile     ('t', "configFile", "config-file", "MM configuration file name",CmdArg::isREQ);
  CmdArgInt cmModeDis      ('v', "commonModeDisabled", "common-mode-disabled", "MM disable common mode");

  CmdArgInt publish_update_cycle_seconds    ('u', "update", "publish_update_cycle_seconds", "number of events to be processed before publish (default 10000)\n"
                                                         "-1 means work forever" );
   
//defaults
  partition_name = getenv("TDAQ_PARTITION");
  events = -1;
  runType = "physics";
  chamberName = "ExMe2";
  stripNumber = -1;
  multiFileRun = true;    // !!!

//   datafilename = "";
  pedestalFile = "";
  cmModeDis = 0;
  publish_update_cycle_seconds = 5;
    
  CmdArgvIter arg_iter( --argc, ++argv );
  CmdLine cmd(*argv, &partition_name, &events, &chamberName, &stripNumber, &runType, &datafilename, &multiFileRun,
              &pedestalFile, &configFile, &cmModeDis, &publish_update_cycle_seconds, NULL);
  cmd.description( "mmCalibration Monitoring" );
  cmd.parse( arg_iter );
 
  if (strcmp(runType,"physics") == 0) {
  std::cout << " run type = " << string(runType) << std::endl;
     m_preset_event_type = CEvent::CEvent::eventTypePhysics;
     m_preset_run_type = CConfiguration::runtypePhysics;
  }
  if (strcmp(runType,"pedestal") == 0) {
  std::cout << " run type = " << string(runType) << std::endl;
     m_preset_event_type = CEvent::eventTypePedestals;
     m_preset_run_type = CConfiguration::runtypePedestals;;
  }

// prepare argc, argv for the MM configuration
  char argvConfig[argRows][argColumns] = {"Dummy"};
  char* pArg [10];      // array of pointers to argvConfig
  int argcConfig = prepareArg(argvConfig,pArg,"dummyRootFilename",pedestalFile,configFile,cmModeDis);

  printf(" argcConfig = %d\n",argcConfig);
  for (int i=0; i<argcConfig; i++) {
    printf(" i = %d,  pArg[] = %s\n",i,pArg[i]);
  }

  try {
    // create the MM objects
    init_mm(argcConfig, pArg);
  
    // initialize TDAQ IPC
    IPCCore::init( argc, argv );
  } 
  catch( daq::ipc::Exception & ex ) {
    ers::fatal( ex );
  }
  catch ( runtime_error &ex ) {
    cerr << ex.what() << endl;
    return 1;
  }

  signal(SIGTERM, term_handler);
  IPCPartition partition(partition_name);

  EventLooperCalibration evLoop;
 
// transmit to EventLooper
  evLoop.SetDataContainerPtr(&m_datacontainer);
  evLoop.SetChamberName(string(chamberName));
  evLoop.SetStripNumber(stripNumber);
  evLoop.SetEventContainerPtr(&m_eventcontainer);

  evLoop.SetConfiguration(m_config);
  evLoop.SetEventDecoder(m_decoder);
  evLoop.SetEventVectors(m_eventVectors);

  try {
    if(datafilename.isNULL()) {
      OHRootProvider provider = OHRootProvider(partition, "Histogramming", "RCDMonitor");
      evLoop.SetHistProvider(provider);
      evLoop.SetPublishCycleTime(publish_update_cycle_seconds);
      evLoop.ProcessEvents<DAQEventReader, IPCPartition>(partition, events, multiFileRun);
    } else { 
      evLoop.ProcessEvents<FileEventReader, string>(string(datafilename), events, multiFileRun);
    }
  }
  catch (exception &e) {
    cerr << "ERROR: " << e.what() << endl;
  }
  catch (string &s) {
    cerr << "ERROR: " << s << endl;
  }

  delete_mm();

  std::cout << " mmCalibration " << " going to return " << std::endl;

  return 0;
}

void init_mm( int argcConfig, char* pArg []) {

// construct MM objects: config, decoder, writer and event vectors
  try {
    m_config = new CConfiguration(argcConfig, pArg, "dummy");
  }
  catch (std::string str) {
     throw runtime_error(string("Errors in Configuration: ") + str);
  }
  if (m_config->error()) {
     throw runtime_error("Other Errors in configuration: ");
  }

  m_save_data_flag = true;             // REVIEW

  m_config->run_type(m_preset_run_type);

  std::cout << " init_mm " << " &m_datacontainer = " << &m_datacontainer << std::endl;
  std::cout << " init_mm " << " &m_eventcontainer = " << &m_eventcontainer << std::endl;

  m_decoder = new CEventDecoder(m_config, &m_datacontainer, &m_eventcontainer);
//  m_decoder->attach(m_receiverFile);

  m_decoder->preset_event_type(m_preset_event_type);

  m_eventVectors = new CEventVectors(m_config, &m_eventcontainer, m_save_data_flag);

  std::cout << " eventDisplay, CEventVectors OK " << std::endl;

}

void delete_mm() {

// destruct MM objects: config, decoder, writer and event vectors
  delete m_eventVectors;
  delete m_decoder;
  delete m_config;
}


void 
term_handler(int param) {
  assert(param == 15);
  std::cout << "Handling SIGTERM... exiting" << std::endl;
  exit(0);
}
