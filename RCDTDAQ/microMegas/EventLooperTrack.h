#ifndef __EVENT_LOOPER_TRACK_H__
#define __EVENT_LOOPER_TRACK_H__

#include "RCDRawEvent.h"
#include "DAQEventReader.h"
#include "FileEventReader.h"

#include <oh/OHRootProvider.h>
#include <TFile.h>
#include <TH1.h>

#include <CUDPData.h>
#include "CEventDecoder.h"

#include <time.h>
// #include <sstream>
#include <vector>

#include "MonitorTrack.h"

class EventLooperTrack {
  public:
    EventLooperTrack();
    template <typename EventReader, typename Source>
    void ProcessEvents(Source src, int nEvents, bool multiFileRun);

    void SetPublishCycleTime(long int cycleTime) { m_publishCycleTime = cycleTime; }
    void SetHistProvider(OHRootProvider& prov) 
    {
      m_histProvider = &prov;
      m_publishHists = true;
    } 

    void SetDataContainerPtr(std::list <CUDPData*>* dc) {m_datacontainerPtr =  dc; }
    void SetEventContainerPtr(std::list <CMMEvent*>* ec) {m_eventcontainerPtr =  ec; }

    void SetConfiguration(CConfiguration* conf) {m_config =  conf; }
    void SetEventDecoder(CEventDecoder* evd) {m_decoder = evd; }
    void SetEventVectors(CEventVectors* evv) {m_eventVectors =  evv; }

    bool IsTimeToPublish();

    template <typename Monitor>
    void PublishHists(Monitor& monitor);

  private:
    OHRootProvider* m_histProvider;
    bool m_publishHists;
    std::string m_dataFile;
    long int m_publishCycleTime;
    time_t m_lastPublicationTime;

    std::list <CUDPData*>*        m_datacontainerPtr;
    std::list <CMMEvent*>*        m_eventcontainerPtr;

    CConfiguration*      m_config;
    CEventDecoder*       m_decoder;
    CEventVectors*       m_eventVectors;

};


#endif
