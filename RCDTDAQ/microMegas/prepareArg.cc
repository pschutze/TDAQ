/*
 *  prepareArg.cpp
 *
 *  Created by J.O.Petersen 17/03/28
 *
 */

#include "argArray.h"

#include <cmdl/cmdargs.h>

int prepareArg(char argvConfig[argRows][argColumns],char* pArg[], const char rfn[], const CmdArgStr pedestalFile,
               const CmdArgStr configFile, const int cmModeDis) {

// prepare argc, argv for MM configuration
  int argcConfig = 1;

  strcpy(argvConfig[0],"Dummy");
  pArg[0] = argvConfig[0];              // not used in CConfiguration

  strcpy(argvConfig[argcConfig], "-uts:");
  strcat(argvConfig[argcConfig],rfn);
  pArg[argcConfig] = argvConfig[argcConfig];
  argcConfig++;

  if( strlen(pedestalFile) > 0) {
    strcpy(argvConfig[argcConfig], "-p:");
    strcat(argvConfig[argcConfig],pedestalFile);
    pArg[argcConfig] = argvConfig[argcConfig];
    argcConfig++;
  }
  if( strlen(configFile) > 0) {
    strcpy(argvConfig[argcConfig], "--config:");
    strcat(argvConfig[argcConfig], configFile);
    pArg[argcConfig] = argvConfig[argcConfig];
    argcConfig++;
  }
  if( cmModeDis > 0) {          // transmit IF true
    strcpy(argvConfig[argcConfig], "--common_mode_disabled");
    pArg[argcConfig] = argvConfig[argcConfig];
    argcConfig++;
  }

/********************************
  printf(" argcConfig = %d\n",argcConfig);
  printf(" argvConfig = %s\n",argvConfig[0]);
  for (int i=0; i<argcConfig; i++) {
    printf(" i = %d,  argvConfig[] = %s\n",i,argvConfig[i]);
    printf(" i = %d,  pArg[] = %s\n",i,pArg[i]);
  }
**********************************/
  return argcConfig;
}

