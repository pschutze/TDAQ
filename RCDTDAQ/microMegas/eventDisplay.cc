/**************************************************/
/*  Event Display 			          */
/* runs as a TApplication			  */
/*                                                */
/* is a model for monitoring programs:            */
/* contains basic code fragments		  */
/*						  */
/*  author:  J.Petersen 		          */
/*  2017/03/28					  */
/*  adapt to zero suppression Jan. 2018           */
/**************************************************/

#include <unistd.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <boost/format.hpp>

#include <time.h>

#include <cmdl/cmdargs.h>
#include <ipc/core.h>

#include "DAQEventReader.h"
#include "FileEventReader.h"

// MM classes
#include "CConfiguration.h"
#include "CEventDecoder.h"
#include "CEventVectors.h"
#include "CUDPData.h"
#include "CEvent.h"

#include <TEnv.h>
#include <TString.h>
#include <TApplication.h>
#include "TCanvas.h"
#include "TGraph.h"
#include "TGraph2D.h"
#include "TH2D.h"

#include "eventDisplay.h"
#include "argArray.h"

// globals ..
// why not just  class variables?
bool g_onlineFlag = true;
bool g_pedestalSubtraction;
std::map<int, double>* g_pedestals_stdev;   // ONLY if pedestal subtraction

TString g_chamberNameTstr;
/************************************/
int g_sigma_cut_factor = 4;	// not used here 17/4/17
/************************************/
TCanvas* g_canvas0;
TCanvas* g_canvas2;
TCanvas* g_canvas3;
TH2I* g_apvQ;
TGraph* g_minAtStrip;
TGraph* g_minAtTime;
TGraph* g_maxAtStrip;
TGraph* g_maxAtTime;
TH2I* g_maxAtStripTime;

using namespace std;

template<typename EventReader, typename Source>
void ProcessEvents(Source, int nEvents);
int buildEventVectors(std::vector<unsigned int>& rawEvent);
void buildFrameContainer(std::vector<unsigned int>& rawEvent, std::list <CUDPData*>* m_datacontainer);
int prepareArg(char argvConfig[argRows][argColumns], char* pArg[], const char rfn[], const CmdArgStr pedFile,
               const CmdArgStr configFile, const int cModeDis);
void init_mm( int argcConfig, char* pArg []);
void plotHistos ();
int getMaxStrips(CmdArgStr chStr);
void term_handler(int);

int 
main(int argc, char** argv)
{
  std::cout << " main: " << "argv[0] = " << argv[0] << std::endl;

  CmdArgStr partition_name ('p', "partition", "partition-name", "partition to work in." );
  CmdArgInt events         ('e', "events", "events-number", "number of events to retrieve (default 1)\n"
                                                         "-1 means work forever" );
  CmdArgStr chamberName    ('c', "chamberName", "chamber-name", "microMegsa chamber name");
  CmdArgStr runType        ('r', "runType", "run-type", "run type: physics/pedestal");

  CmdArgStr datafilename   ('f', "datafilename", "data-file-name", "Data file name, read events from the file instead of online stream");
   
  CmdArgStr pedestalFile   ('s', "pedestalFile", "file-name", "MM pedestal file name");
  CmdArgStr configFile     ('t', "configFile", "config-file", "MM configuration file name",CmdArg::isREQ);

//defaults
  partition_name = getenv("TDAQ_PARTITION");
  events = 1;
  runType = "physics";
  chamberName = "ExMe2";

  datafilename = "";
  pedestalFile = "";

  CmdArgvIter arg_iter( --argc, ++argv );
  CmdLine cmd(*argv, &partition_name, &events, &chamberName, &datafilename, &runType, 
                     &pedestalFile, &configFile, NULL);
  cmd.description( "eventDisplay monitor program" );
  cmd.parse( arg_iter );

  g_chamberNameTstr = TString(chamberName);

/*****************************
  std::cout << " chamber name = " << g_chamberNameTstr << std::endl;
  std::cout << " run type = " << string(runType) << std::endl;
  std::cout << " datafilename = " << string(datafilename) << std::endl;
  std::cout << " len of datafilename = " << strlen(datafilename) << std::endl;
  std::cout << " pedestalFile = " << string(pedestalFile) << std::endl;
  std::cout << " len of pedestalFile = " << strlen(pedestalFile) << std::endl;
  std::cout << " configFile = " << string(configFile) << std::endl;
  std::cout << " len of config File = " << strlen(configFile) << std::endl;
**************************/

//  TApplication interferes with argc, argv : avoid same command definitions. See TApplication documentation.
// this should be AFTER the command line parsing above
  TApplication *myapp = new TApplication("EventDisplay", &argc, argv);

  if (strcmp(runType,"physics") == 0) {
  std::cout << " run type = " << string(runType) << std::endl;
     m_preset_event_type = CEvent::CEvent::eventTypePhysics;
     m_preset_run_type = CConfiguration::runtypePhysics;
  }
  if (strcmp(runType,"pedestal") == 0) {
  std::cout << " run type = " << string(runType) << std::endl;
     m_preset_event_type = CEvent::eventTypePedestals;
     m_preset_run_type = CConfiguration::runtypePedestals;;
  }

// prepare argc, argv for the MM configuration
  char argvConfig[argRows][argColumns] = {"Dummy"};
  char* pArg [10];	// array of pointers to argvConfig
  int argcConfig = prepareArg(argvConfig,pArg,"dummyRootFilename",pedestalFile,configFile,0);

  printf(" argcConfig = %d\n",argcConfig);
  for (int i=0; i<argcConfig; i++) {
    printf(" i = %d,  pArg[] = %s\n",i,pArg[i]);
  }

// create the MM objects
  init_mm(argcConfig, pArg);

  m_maxStrips = getMaxStrips(chamberName);

  try {
    IPCCore::init( argc, argv );
  } catch( daq::ipc::Exception & ex ) {
    ers::fatal( ex );
  }

 signal(SIGTERM, term_handler);
  IPCPartition partition(partition_name);

  time(&m_run_start_time);	// start of THIS "ROOT run"

  g_canvas2 = new TCanvas("canvas2", "c2", 200,200,700,500);
  g_canvas2->Divide(2,1);
  g_canvas3 = new TCanvas("canvas3", "c3", 100,100,700,500);
  g_canvas0 = new TCanvas("canvas0", "c0", 300,300,700,500);

  if (strlen(datafilename) == 0) {
   std::cout << " Online monitoring" << std::endl;
   g_onlineFlag = true;
   ProcessEvents<DAQEventReader, IPCPartition>(partition, events);
  }
  else {
   std::cout << " From file monitoring" << std::endl;
   g_onlineFlag = false;
   ProcessEvents<FileEventReader, string>(string(datafilename), events);
  }

  plotHistos();

//  myapp->Run(kTRUE);   // segfaults after exit ??
  myapp->Run();

  delete m_eventVectors;
  delete m_decoder;
  delete m_config;

  std::cout << " eventDisplay, going to exit" << std::endl;

  return 0;
}

template <typename EventReader, typename Source>
void
ProcessEvents(Source src, int events) {

  vector<unsigned int> rawEvent;
  EventReader evReader(src);
  map<int,int> stripToChannel;

  g_pedestalSubtraction = !(string(m_config-> pedestal_filename()).empty());
  cout << "ProcessEvents" << " g_pedestalSubtraction = " << g_pedestalSubtraction << endl;

// not used in this program .. 17/4/17
  if (g_pedestalSubtraction == 0) {
    g_pedestals_stdev = (std::map<int, double>*)m_config->pedestal_stdev_map();
    std::cout << " Stdev s " <<  " size = " << g_pedestals_stdev->size() << std::endl;
//    for(auto it = g_pedestals_stdev->begin(); it != g_pedestals_stdev->end(); ++it ) {
//      std::cout << it->first << " " << it->second << "\n";
//    }
  }

  cout << "ProcessEvents" << "Number of events: " << events << endl;
  int eventCount = 0;
  while (eventCount < events || events == -1) {		// event loop
    cout << "\n # Event No.: " << eventCount << " events argument = " << events << endl;
    rawEvent = evReader.GetNextRawEvent();

    for(const unsigned int& it : rawEvent) {
//      cout << boost::format("%08x") % it << " "; 
    }

  int apv_ptr = 9 ;  // RODHDRSIZE
  std::string ap(1,(char)((rawEvent[apv_ptr+3] & 0x00ff0000)>>16));
  if (ap == "Z") {
    m_zsEnable = 1;   // set at RUN time
    std::cout << " ZS mode " << std::endl;
  }

// specific code for ONE event monitoring programs
  if ((g_onlineFlag == true) && (eventCount == 1)) break;
  if ((g_onlineFlag == false) && (eventCount < (events-1))) {   // start at event 'eventCount'
    ++eventCount;
    continue;
  }

  int retBuild = buildEventVectors(rawEvent);
  if( retBuild != CEventVectors::RETURN_OK) {
    std::cout << " ProcessEvents " << " retBuild = " << retBuild << std::endl;
    exit(0);
  }

  unsigned int apv_evt =                        m_eventVectors->get_apv_evt();
  std::vector <unsigned int> apv_fec =  	m_eventVectors->get_apv_fec();
  std::vector <unsigned int> apv_id  =  	m_eventVectors->get_apv_id();
  std::vector <unsigned int> apv_ch  =  	m_eventVectors->get_apv_ch();
  std::vector <std::string>  mm_id = 		m_eventVectors->get_mm_id();
  std::vector <unsigned int> mm_readout =	m_eventVectors->get_mm_readout();
  std::vector <unsigned int> mm_strip	 =	m_eventVectors->get_mm_strip();
  std::vector <std::vector <short> > apv_q =	m_eventVectors->get_apv_q();
  std::vector <short> apv_qmax =                m_eventVectors->get_apv_qmax();
  std::vector <short> apv_tbqmax =              m_eventVectors->get_apv_tbqmax();
 
  std::cout << " apv_evt = " << apv_evt << std::endl;

  std::cout << " size of apv_fec = " << apv_fec.size() 
            << " fecNo[0] = " << apv_fec[0]  << std::endl;

  std::cout << " size of apv_id = " << apv_id.size()
            << " id[0] = " << apv_id[0]
            << " id[apv_id.size()-1] = " << apv_id[apv_id.size()-1]
            << std::endl;

  std::cout << " size of apv_ch = " << apv_ch.size()
            << " ch[0] = " << apv_ch[0]  << std::endl;

  std::cout << " size of mm_id = " << mm_id.size()
            << " mm_id[0] = " << mm_id[0]
            << " mm_id[mm_id.size()-1] = " << mm_id[mm_id.size()-1]  << std::endl;

  std::cout << " size of mm_readout = " << mm_readout.size()
            << " mm_readout[0] = " << mm_readout[0]
            << " mm_readout[mm_readout.size()-1] = " << mm_readout[mm_readout.size()-1]  << std::endl;

  std::cout << " size of mm_strip = " << mm_strip.size()
              << " mm_strip[0] = " << mm_strip[0]
              << " mm_strip[mm_strip.size()-1] = " << mm_strip[mm_strip.size()-1]  << std::endl;

  std::cout << " size of apv_tbqmax = " << apv_tbqmax.size()
              << " apv_tbqmax[0] = " << apv_tbqmax[0]
              << " apv_tbqmax[mm_strip.size()-1] = " << apv_tbqmax[mm_strip.size()-1]  << std::endl;

  // create strip to channel MAP for ONE chamber
  // NB: in ZS mode, the stripChannel map may be empty for this chamber
  for (int ii = 0; ii<mm_strip.size(); ii++) {  // ii = apvChannel
//    std::cout << " mm_id[ii] = " << mm_id[ii] << std::endl;
    if (mm_id[ii] == string(g_chamberNameTstr.Data())) {
      int strip = mm_strip[ii];
//      std::cout << " strip = " << strip << std::endl;
      stripToChannel[strip] =  ii;
    }
  }
  std::cout << " size of stripToChannel = " << stripToChannel.size() << std::endl;
  if (stripToChannel.size() == 0) {
    std::cout << " no channels in this chamber" << std::endl;
    exit(0);
  }

  int apvCh = 0;
  int apvChannelFirst = 0;
  int timeBin = 0;
  int timeBinMax = 0;
  bool firstTime = true;

  int ii=0;
  int jj=0;
  int iMinStrip,jMinTime,minApvChannel; // no pedestal subtraction
  int iMaxStrip,jMaxTime,maxApvChannel; // pedestal subtraction or zero suppression
  float minCharge, maxCharge;

  minCharge = 3000.0;
  maxCharge = -100.0;

  timeBinMax = apv_q[0].size();
  std::cout << " timeBinMax = " << timeBinMax << std::endl;

// define histo when dimensions known 
  g_apvQ = new TH2I("g_apvQ",
           " Chamber "  + g_chamberNameTstr + " Charge of all APV measurements; strip number;timeBin(25 ns);charge",
           m_maxStrips,1,m_maxStrips+1, timeBinMax, 0, timeBinMax);   // 1024 for BL4S chambers

  for (auto row = apv_q.begin(); row != apv_q.end(); ++row)
  {

    if (g_pedestalSubtraction && m_zsEnable == 0) {
// model for finding pedestal STD for a strip, channel
      std::vector<int16_t>::const_iterator imax = max_element(row->begin(), row->end());
//    std::cout << " ProcessEvents " << " apvCh = " << apvCh << " Q max = " << *imax << std::endl;
      int apvChNo = apvCh&0x7f;  //  channel number from 0 to 127 !!
      int apvChId = ((((apv_fec[apvCh]<<4) | apv_id[apvCh]) << 8) | apvChNo);	// copy makeChannelId
//    std::cout << " ProcessEvents " << " apvChId = " << apvChId << std::endl;
      double pedStd = (*g_pedestals_stdev)[apvChId];
//      std::cout << " ProcessEvents " << " pedStd = " << pedStd << std::endl;

    }

// one apvChannel (27 timebins)
       for (auto col = row->begin(); col != row->end(); ++col)
       {
//         std::cout << apvChannel << " " << timeBin << " " << *col << "  " ;
//         std::cout << "index = " << apvCh*timeBinMax + timeBin << " ";
//         std::cout << "mm_id = " << mm_id[apvCh]  <<  " ";

           if (mm_id[apvCh] == string(g_chamberNameTstr.Data())) {
             if (firstTime) {  // points in graphs: 0,1,2.....,
               apvChannelFirst = apvCh;
               firstTime = false;
             }
             int stripNo = mm_strip[apvCh];
//           std::cout << "apvChannel = " << apvCh << " ";
//           std::cout << "index = " << (apvCh - apvChannelFirst)*timeBinMax + timeBin << " ";
//           std::cout << stripNo << " " << timeBin << " " << *col << "  " ;
             if (*col > 3200 ) {
                std::cout << stripNo << " " << timeBin << " " << *col << "  " ;
             }
             g_apvQ->Fill(stripNo, timeBin, *col);

             if (!g_pedestalSubtraction && m_zsEnable == 0) {
               if (*col < minCharge) {
                 minCharge = *col; minApvChannel = apvCh; iMinStrip = mm_strip[apvCh]; jMinTime = timeBin;
               }
             }
             else { // pedestal subtraction OR ZS
               if (*col > maxCharge) {
                 maxCharge = *col; maxApvChannel = apvCh; iMaxStrip = mm_strip[apvCh]; jMaxTime = timeBin;
               }
             }
           }

           timeBin++;
       }
//       timeBinMax = timeBin;
       timeBin = 0;
       apvCh++;
   }

  if (!g_pedestalSubtraction && m_zsEnable == 0) {
    std::cout << " minimum strip = " << iMinStrip << " min timebin = "
             << jMinTime << " min charge =  " << minCharge
             << " minApvChannel = " << minApvChannel << std::endl;

    TString minString;
    minString.Form("%d",iMinStrip);
    TString minTime;
    minTime.Form("%d",jMinTime);

    g_minAtStrip = new TGraph();
    g_minAtStrip->SetTitle(" Chamber " + g_chamberNameTstr +  " Strip # " + minString +
                          " Charge; timeBin(25 ns);charge");

    g_minAtTime = new TGraph();
    g_minAtTime->SetTitle(" Chamber " + g_chamberNameTstr +  " Time Bin " + minTime +
                          " Charge; strip number;charge");

// one strip(channel)
    for (int k=0; k<timeBinMax; k++) {
      g_minAtStrip->SetPoint(k, k, apv_q[minApvChannel][k]);
    }

// one column: strips are non-consecutive in rows ..
     int kmin = -10;
     int kmax=  10;
     for (int k=kmin; k<kmax; k++) {
// protect against strips outside chamber
       if (stripToChannel.find(iMinStrip+k) != stripToChannel.end()) {
         int channel = stripToChannel[iMinStrip+k];

//       std::cout << " k-kmin = " << (k-kmin) << " iMinStrip+k = " << (iMinStrip+k)
//                 << " channel = " << channel << " jMinTime = " << jMinTime << std::endl;

          g_minAtTime->SetPoint(k-kmin, iMinStrip+k, apv_q[channel][jMinTime]);
       }
     }
   }

   if (g_pedestalSubtraction || m_zsEnable == 1) {             // look for maxima ..
     std::cout << " maximum strip = " << iMaxStrip << " max timebin = "
               << jMaxTime << " max charge =  " << maxCharge
               << " maxApvChannel = " << maxApvChannel << std::endl;

     TString maxString;
     maxString.Form("%d",iMaxStrip);
     TString maxTime;
     maxTime.Form("%d",jMaxTime);

     g_maxAtStrip = new TGraph();
     g_maxAtStrip->SetTitle(" Chamber " + g_chamberNameTstr +  " Strip # " + maxString +
                           " Charge; timeBin(25 ns);charge");

// one strip(channel)
     for (int k=0; k<timeBinMax; k++) {
       g_maxAtStrip->SetPoint(k, k, apv_q[maxApvChannel][k]);
     }

     g_maxAtTime = new TGraph();
     g_maxAtTime->SetTitle(" Chamber " + g_chamberNameTstr +  " Time Bin " + maxTime +
                         " Charge; strip number;charge");

// one column: strips are non-consecutive in rows ..
     int kmin = -10;
     int kmax =  10;
     int ll = 0;
     for (int k=kmin; k<kmax; k++) {
// protect against missing strips 
       if (stripToChannel.find(iMaxStrip+k) != stripToChannel.end()) {
          int channel = stripToChannel[iMaxStrip+k];

//       std::cout << " k-kmin = " << (k-kmin) << " iMaxStrip+k = " << (iMaxStrip+k)
//                 << " channel = " << channel << " jMaxTime = " << jMaxTime << std::endl;

          g_maxAtTime->SetPoint(ll, iMaxStrip+k, apv_q[channel][jMaxTime]);
          ll++;
       }
     }

     kmin = -10;
     kmax =  10;
     g_maxAtStripTime = new TH2I("g_maxAtStripTime",
           " Chamber " + g_chamberNameTstr  + " Charge; strip number;timeBin(25 ns);charge",
           20,iMaxStrip+kmin,iMaxStrip+kmax, timeBinMax, 0, timeBinMax);

     for (int k=kmin; k<kmax; k++) {
// protect against strips outside chamber
       if (stripToChannel.find(iMaxStrip+k) != stripToChannel.end()) {
         int channel = stripToChannel[iMaxStrip+k];

         for (int ll=0; ll<timeBinMax; ll++) {
//           std::cout << " (k-kmin)*timeBinMax+ll = " << (k-kmin)*timeBinMax+ll << " iMaxStrip+k = " << (iMaxStrip+k)
//                     << " channel = " << channel << " apv_q[channel][ll] = " << apv_q[channel][ll] << std::endl;
          g_maxAtStripTime->Fill( iMaxStrip+k, ll, apv_q[channel][ll]);
         }
       }
     }

   }

//   plotHistos();

   ++eventCount;
   cout << "\n" << endl;
 }
}

void init_mm( int argcConfig, char* pArg []) {

// construct MM objects: config, decoder, writer and event vectors
  try {
    m_config = new CConfiguration(argcConfig, pArg, "dummy");
  }
  catch (std::string str) {
     throw runtime_error(string("Errors in Configuration: ") + str);
  }
  if (m_config->error()) {
     throw runtime_error("Other Errors in configuration: ");
  }

  m_save_data_flag = true;             // REVIEW

  m_config->run_type(m_preset_run_type);

  m_decoder = new CEventDecoder(m_config, &m_datacontainer, &m_eventcontainer);
//  m_decoder->attach(m_receiverFile);

  m_decoder->preset_event_type(m_preset_event_type);

  m_eventVectors = new CEventVectors(m_config, &m_eventcontainer, m_save_data_flag);

  std::cout << " eventDisplay, CEventVectors OK " << std::endl;

}

void 
term_handler(int param) {
  assert(param == 15);
  std::cout << "Handling SIGTERM... exiting" << std::endl;
  exit(0);
}

int buildEventVectors(std::vector<unsigned int>& rawEvent) {

  int err = 0;

  buildFrameContainer(rawEvent,&m_datacontainer );
//  std::cout << " buildEventVectors " << " size of data container = " << (*m_datacontainer).size() << std::endl;

  err = m_decoder->buildEventContainer();
  std::cout << " ProcessEvents" << " after buildEventContainer, err = " << err << std::endl;

//  std::cout << " ProcessEvents " << " size of event container = " << (*m_eventcontainer).size() << std::endl;

  err = m_eventVectors->buildVectors();

  std::cout << " ProcessEvents" << " after buildVectors(), err = " << err << std::endl;

  return err;

}

// see getGeometry() in eventDiaplayTrack
int getMaxStrips(CmdArgStr chamberName) {

  TEnv* tenv = (TEnv*)m_config->get_tenv_config();
  stringstream ss;
  ss.str("");
  ss << "mmdaq.Chamber." << chamberName << ".Strips.X.Max";
//   cout << " ss = " << ss.str() << endl;
//   cout << " ss.str().c_str() = " << ss.str().c_str() << endl;
  string str1 = tenv->GetValue(ss.str().c_str(), "");
  int maxStrips = stoi(str1);
  cout << " getMaxStrips:  chamber name = " << chamberName << " max strips = "  << maxStrips << endl;
  return maxStrips;
}

void plotHistos () {

  if (!g_pedestalSubtraction && m_zsEnable == 0) {
    g_canvas2->cd(1);
    g_minAtStrip->Draw("AL*");
    g_canvas2->cd(2);
    g_minAtTime->Draw("AL*");
    g_canvas2->Update();
  }

  if (g_pedestalSubtraction || m_zsEnable == 1) {
    g_canvas2->cd(1);
    g_maxAtStrip->Draw("AL*");
    g_canvas2->cd(2);
    g_maxAtTime->Draw("AL*");
    g_canvas2->Update();
  }
 
  if (g_pedestalSubtraction || m_zsEnable == 1) {
    g_canvas3->cd();
    g_maxAtStripTime->GetXaxis()->SetTitleOffset(1.5);
    g_maxAtStripTime->GetYaxis()->SetTitleOffset(1.5);
    g_maxAtStripTime->Draw("LEGO");
    g_canvas3->Update();
  }

  g_canvas0->cd();
  g_apvQ->GetXaxis()->SetTitleOffset(1.5);
  g_apvQ->GetYaxis()->SetTitleOffset(1.5);
//  g_apvQ->Draw("LEGO");
  g_apvQ->Draw("SURF1");
  g_canvas0->Update();

}
