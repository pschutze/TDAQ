#include <boost/format.hpp>
#include "EventLooper.h"

EventLooper::EventLooper():
  m_histProvider(NULL),
  m_publishHists(false),
  m_publishCycleTime(30),
  m_dataFile(""),
  m_timeLow(0),
  m_timeHigh(1e9)
{
  m_lastPublicationTime = time(NULL);
}

template <typename EventReader, typename Source>
void 
EventLooper::ProcessEvents(Source src, int nEvents, bool multirun)
{
  EventReader evReader(src,multirun);
  cout << " EventLooper::ProcessEvents " << " run number = " << evReader.GetRunNumber() << endl;
  cout << " EventLooper::ProcessEvents " << " m_publishHists = " << m_publishHists << endl;

  std::vector<std::unique_ptr<MonitorBase>> vMonitors;
  std::unique_ptr<MonitorChamber> mChamber(new MonitorChamber("mChamber", m_config, m_chamberName, m_stripNumber, 
                                                              to_string(evReader.GetRunNumber()), m_publishHists));

  // transmit to Monitor 
  mChamber->SetDataContainerPtr(m_datacontainerPtr);
  mChamber->SetEventContainerPtr(m_eventcontainerPtr);

  mChamber->SetEventDecoder(m_decoder);
  mChamber->SetEventVectors(m_eventVectors);

  mChamber->SetHistProvider(m_histProvider);
  mChamber->SetTimeRange(m_timeLow, m_timeHigh);

  vMonitors.push_back(std::move(mChamber));

  std::unique_ptr<MonitorBeam> mBeam(new MonitorBeam("mBeam", m_config, to_string(evReader.GetRunNumber()), m_publishHists));

  // transmit to Monitor
  mBeam->SetDataContainerPtr(m_datacontainerPtr);
  mBeam->SetEventContainerPtr(m_eventcontainerPtr);

  mBeam->SetEventDecoder(m_decoder);
  mBeam->SetEventVectors(m_eventVectors);

  mBeam->SetHistProvider(m_histProvider);

  vMonitors.push_back(std::move(mBeam));
 
/***********************************************  irrelevant for T9  
  if (m_publishHists) { // only ONLINE, TEST on file: comment out
    std::unique_ptr<MonitorQmaxRealTime> mQmaxRealTime(
          new MonitorQmaxRealTime("mQmaxRealTime", m_config, m_chamberName));

  // transmit to Monitor 
    mQmaxRealTime->SetQmaxMeanTime(m_qmaxMeanTime);
    mQmaxRealTime->SetDataContainerPtr(m_datacontainerPtr);
    mQmaxRealTime->SetEventContainerPtr(m_eventcontainerPtr);

    mQmaxRealTime->SetEventDecoder(m_decoder);
    mQmaxRealTime->SetEventVectors(m_eventVectors);

    mQmaxRealTime->SetHistProvider(m_histProvider);

    vMonitors.push_back(std::move(mQmaxRealTime));

  } // to TEST QMaxRealtime on ROOT files : comment out
***************************************/

  cout << " # monitors = " << vMonitors.size() << endl;

  cout << "Number of events: " << nEvents << endl;
  int eventCount = 0;
  while (eventCount < nEvents || nEvents == -1) {
    if (eventCount%1000 == 0 or IsTimeToPublish()){
      cout << "\n # Event No.: " << eventCount << endl;
    }
    vector<unsigned int> rawEvent = evReader.GetNextRawEvent();
//    std::cout << "EventLooper::ProcessEvents " << " rawEvent size = " << rawEvent.size() << std::endl;
    int evSize = rawEvent.size();
    if(rawEvent.size() == 0)  // Enf Of File
      break;
    // skip events with status ne 0
    int status = rawEvent[evSize-4];  // REVIEW with new FileEventReader
    if (status !=0) {
      std::cout << " status = " << status << " event size = " << evSize << " eventCount = " << eventCount << std::endl;
      continue;
    }

//    RCDRawEvent ev = RCDRawEvent(rawEvent);aa NOT relevant to microMegas!

//    for(const unsigned int& it : rawEvent) {
//      cout << boost::format("%08x") % it << " ";
//    }

    for(auto & monitor : vMonitors) {
      monitor->Process(rawEvent);
//      monitor->Print(ev);
    }

    if(m_publishHists && IsTimeToPublish())
    {
      for(auto & monitor : vMonitors)
      monitor->PublishHists();
    }
    ++eventCount;
  }

  if (!m_publishHists) {
    for(auto & monitor : vMonitors) {
      monitor->WriteToFile();
      std::cout << "EventLooper::ProcessEvents " << " ROOT file written " << std::endl;
    }
  }
}

template void EventLooper::ProcessEvents<DAQEventReader, IPCPartition>(IPCPartition, int, bool);
template void EventLooper::ProcessEvents<FileEventReader, std::string>(std::string, int, bool);

bool EventLooper::IsTimeToPublish()
{
  time_t currentTime = time(NULL);
  if( static_cast<long int>(currentTime-m_lastPublicationTime) < m_publishCycleTime)
    return false;
  m_lastPublicationTime = time(NULL);
  return true;
}
