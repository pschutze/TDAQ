#ifndef __MONITOR_CLUSTER__ 
#define __MONITOR_CLUSTER__ 

#include "MonitorBase.h"
#include "Cluster.h"

#include <vector>
#include <list>
#include <map>
#include <random>

#include <TH1.h>
#include <TH2.h>
#include "TGraph.h"
#include <TFile.h>

class CUDPData;
class CEventDecoder;
class CEventVectors;

 class MonitorCluster: public MonitorBase {
// class MonitorCluster {
  public:
    MonitorCluster();
    MonitorCluster(std::string name, CConfiguration* config, std::string runNumber, bool publish);
    virtual ~MonitorCluster();

    void SetDataContainerPtr(std::list <CUDPData*>* dc) {m_datacontainerPtr =  dc; }
    void SetEventContainerPtr(std::list <CMMEvent*>* ec) {m_eventcontainerPtr =  ec; }

    void SetHistProvider(OHRootProvider* prov) { m_histProvider = prov; }

    void SetEventDecoder(CEventDecoder* evd) {m_decoder = evd; }
    void SetEventVectors(CEventVectors* evv) {m_eventVectors =  evv; }

    int  buildEventVectors(std::vector<unsigned int>& rawEvent);
    void getGeometry(void);
    void fitAllX(std::vector <std::vector<double> >& xyRel,
                 std::vector <std::vector<double> >& xyRelStripNo);
    std::vector<std::pair<int,int> > findClusterRanges(string chName,
                           std::vector <std::vector <short> >& apv_q,
                           std::vector< std::map<int,int> >& stCh);
    std::vector<Cluster* > makeClusters(string chName,
                         std::vector <std::vector <short> >& apv_q,
                         std::vector<std::pair<int,int> >& cr,
                         std::vector< std::map<int,int> >& stCh);

    virtual bool Process(std::vector<unsigned int>&  ev);
    virtual void Print(std::vector<unsigned int>& ev);
    virtual void WriteToFile();
    virtual void PrepareRootFile(std::string runNumberStr);
    virtual void PublishHists();

/*************  cluster parameters ****************/
// hits must be close in space and time       
// max distance between strips in cluster
// force consecutive strips: don't allow for dead strips !!
    const int CreateClustersMaxGap = 1;
// max distance between QMax times of strips in cluster
    const int CreateClustersMaxTimeBinGap = 4;

/*************************************************/

  private:

    OHRootProvider* m_histProvider;

    bool m_firstEvent;

    TFile * m_outputFile;
    std::string m_MonitorName;

    std::list <CUDPData*>*        m_datacontainerPtr;
    std::list <CMMEvent*>*        m_eventcontainerPtr;

    CConfiguration*      m_config;
    CEventDecoder*       m_decoder;
    CEventVectors*       m_eventVectors;

    std::vector<TH1I*> m_TH1cluster_width;
    std::vector<TH1I*> m_TH1number_clusters;
    std::vector<TH1I*> m_TH1cluster_charge;
    std::vector<TH1I*> m_TH1cluster_qMax;
    std::vector<TH2I*> m_TH2qMaxVsWidth;
    std::vector<TH2I*> m_TH2qMaxVsCharge;
    std::vector<TH1F*> m_TH1xyRel;
    std::vector<TH1F*> m_TH1xyRelStripNo;
    std::vector<TH1F*> m_TH1weightedMinusStrip;
    TH1I* m_TH1number_emptyEvents;
    std::vector<TH1F*> m_resolution;
    std::vector<TH1F*> m_resolutionStripNo;
    TGraph* m_grTrack;
    TGraph* m_grTrackStripNo;

    std::map<std::string,int> m_nameMap;

    std::vector<std::string> m_chamberNames;
    std::map <std::string,float> m_zPos;
    std::map <std::string,float> m_angle;
    std::map <std::string,float> m_pitch;
    std::map <std::string,float> m_maxStrips;

};

  
#endif
