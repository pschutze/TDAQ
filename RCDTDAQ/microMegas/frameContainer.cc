/*
 *  frameContainer.cpp
 *
 *  Created by J.O.Petersen 17/03/28
 *
 * SHOULD GO INTO Commonlibray RCDRawEvent
 *
 */

#include <vector>
#include <list>
#include <time.h>
#include <sys/time.h>
#include <iostream>

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include "CUDPData.h"

const int RODHDRSIZE = 9;

size_t m_received_events_counter;

// transforms a raw event into a container of UDP frames (list)
//void buildFrameContainer(std::vector<unsigned int>& rawEvent, std::list <CUDPData*> & m_datacontainer) {
void buildFrameContainer(std::vector<unsigned int>& rawEvent, std::list <CUDPData*>* m_datacontainer) {

  int ret = 0;
  unsigned long framecounter = 0;
  unsigned int apv_ptr = RODHDRSIZE + 0;

  struct timeval in_time;
  bool got_end_of_event = false;
  gettimeofday(&in_time, NULL);

/*****************************************
// 2018/03/18 ad hoc check on APV data
  std::cout << " buildFrameContainer " << " IP address = " << std::hex << rawEvent[apv_ptr] << std::endl;

  if ((rawEvent[apv_ptr] & 0xffffff00) != 0x0a000000) {  // MUST be 10.0.0.xx
    ret = 1;
    return ret;
  }
*******************************************/

  do {  // loop over APVs
//    std::cout << " buildFrameContainer " << " rawEvent " << " IP address = " << std::hex << rawEvent[apv_ptr]
//                                   << " # words  " << std::dec << rawEvent[apv_ptr + 1]
//                                   << std::dec << std::endl;

    if ( rawEvent[apv_ptr+2] == SRS_NEXT_EVENT ) {  // REVIEW
      got_end_of_event = true;
      ++m_received_events_counter;
      // std::cout << " buildFrameContainer got_end_of_event event = " << m_received_events_counter << std::endl;
    }
    int nWordsInAPV = rawEvent[apv_ptr + 1];
//    std::cout << " buildFrameContainer " << " # bytes = " << nWordsInAPV*sizeof(unsigned int) << std::endl;
    struct sockaddr_in sockIn;
    char ipstr[INET_ADDRSTRLEN];
    sockIn.sin_addr.s_addr = ntohl(rawEvent[apv_ptr]);
    inet_ntop(AF_INET, &(sockIn.sin_addr), ipstr, INET_ADDRSTRLEN);
    //printf(" IP of sender = %s\n",ipstr);

    CUDPData* udpframe = 0;
    try {
      unsigned char* bufPtr = (unsigned char*)&rawEvent[apv_ptr+2];
      udpframe = new CUDPData(bufPtr, nWordsInAPV*sizeof(unsigned int),
                              ipstr, &in_time, framecounter, m_received_events_counter);
    } catch (...) {
        std::cout << "buildFrameContainer : ERROR new CUDPData thrown an exception" << std::endl;
        continue;
    }
    apv_ptr += (nWordsInAPV + 2);
    //std::cout << " buildFrameContainer " << " apv_ptr = " << apv_ptr << std::endl;

    //std::cout << " buildFrameContainer, push UDP frame " << framecounter << std::endl;
    (*m_datacontainer).push_back(udpframe);

    ++framecounter;

  } while(got_end_of_event == false);

//  return ret;
}

