/*
 * adapted to BL4S Jan,17 J.Petersen
 */

#ifndef eventDisplayTrack_h
#define eventDisplayTrack_h

#include <list>
#include <string>
#include <vector>

class CConfiguration;
class CReceiver;
class CReceiverFile;
class CEventDecoder;
class CMMEvent;
class CUDPData;
class CEventVectors;
class CPublisher;

   CConfiguration*	m_config;
   CReceiver*           m_receiver;
   CReceiverFile*       m_receiverFile;
   CEventDecoder*       m_decoder;
   CPublisher*          m_publisher;
   CEventVectors*       m_eventVectors;

   std::list <CUDPData*>        m_datacontainer;
   std::list <CMMEvent*>        m_eventcontainer;

   CEvent::event_type_type      m_preset_event_type;
   CConfiguration::runtype_t    m_preset_run_type;

   int m_zsEnable;

   std::map<int,int> m_stripToChannelChamber;
   std::vector< std::map<int,int> > m_stripToChannel;  // ALL chanbers

   std::vector<std::string> m_chamberNames;   // build our own chamber names
   std::map <std::string,float> m_zPos;
   std::map <std::string,float> m_pitch;
   std::map <std::string,float> m_maxStrips;
   float m_deltaZpos;   // largest distance between chambers

   time_t m_run_start_time;
   bool   m_save_data_flag;

#endif
