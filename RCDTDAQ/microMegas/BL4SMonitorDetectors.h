//  adapt from daq_extras to microMegas
// Jan. 2018 Jorgen Petersen

#ifndef BL4SMONITOR_H
#define BL4SMONITOR_H

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include "TEveGeoShape.h"
#include "TEveBox.h"
#include "TEveLine.h"
#include "TEveManager.h"
#include "TGeoManager.h"
#include "TGeoSphere.h"

//in this file the detectors are derived from the box detector.
using namespace std;

//#define M_PI 3.1415

class BL4SBoxDetector 
{
protected:
  std::string m_name;

  double m_x;
  double m_y;
  double m_z;
  double m_rotation;

  int m_color;

  double m_width;  //x
  double m_height; //y
  double m_length; //z

  double m_xyScale;
  double m_zScale;

public:
  BL4SBoxDetector (std::string name,
                   double xx, double yy, double zz,
                   double pwidth, double pheight, double plength,
                   double xyScale, double zScale,
                   int pcolor);
  ~BL4SBoxDetector();

  //void ShowEvent(BL4SEvent *ev);
  void ShowInfo();
  std::string GetName(){return m_name;}
  double GetWidth(){return m_width;}
  double GetHeight(){return m_height;}
  double GetLength(){return m_length;}
  double GetXPosition(){return m_x;}
  double GetYPosition(){return m_y;}
  double GetZPosition(){return m_z;}
  double GetXYScale(){return m_xyScale;}
  double GetZScale(){return m_zScale;}
  int GetColor(){return m_color;}
  void SetColor(int c){m_color = c;}
  
  void DrawDetector();
};

class ScintillatorMon : public BL4SBoxDetector
{
private:
  bool signal;
public:
  ScintillatorMon (std::string name,
                   double xx, double yy, double zz,
                   double xdim, double ydim, double zdim,          // dimension
                   double xyScale, double zScale );               // scale (x,y) and z
  ~ScintillatorMon();

  void SetScintillatorSignal(bool hit){signal = hit;}
  void ShowScintillatorSignal();
};

class microMegasMon : public BL4SBoxDetector 
{
private:
  double m_xs;      // physical coordinates of hit
  double m_ys;

public:

  microMegasMon (std::string name,
                double xx, double yy, double zz,
                double xdim, double ydim, double zdim,          // dimension
                double xyScale, double zScale);                  // scale (x,y) and z
  ~microMegasMon();

//  void SetmicroMegasSignal(double xx, double yy){xs = xx + m_x; ys = yy + m_y;}
  void SetmicroMegasSignal(double xx, double yy){m_xs = xx; m_ys = yy;}  // REVIEW BL4S
  double GetmicroMegasXSignal(){return m_xs;}
  double GetmicroMegasYSignal(){return m_ys;}
  double GetmicroMegasZSignal(){return m_z;} // ?? REVIEW
  void ShowmicroMegasSignal();
  void ShowmicroMegasXline();
};

class microMegasAddOns 
{
private:

public:
  microMegasAddOns(); 
  ~microMegasAddOns();

  void ShowSignalPath(microMegasMon *dwc0, microMegasMon *dwc1, microMegasMon *dwc2);
  void ShowSignalPathDWC0toDWC1(microMegasMon *dwc0, microMegasMon *dwc1);

};
#endif
