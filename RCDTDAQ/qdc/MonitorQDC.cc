#include <TH1.h>
#include <TFile.h>

#include <oh/OHRootProvider.h>

#include "MonitorQDC.h"
#include "BL4SConfig.h"
#include "CommonLibrary/RCDRawEvent.h"

using namespace std;

const int V792_CHANNELS = 32;

MonitorQdc::MonitorQdc(std::string name, BL4SConfig * cfg, 
                             std::string runNumberStr, bool publish )
{
  m_MonitorName = name;

  // BEFORE booking histograms
  if (!publish) {
    PrepareRootFile(runNumberStr);
  }

/********booking histograms**************/
/* copied from last year ...  */

// QDCs
  for (int channel=0; channel<V792_CHANNELS; channel++) {
    char name[8], title[200];
    sprintf(name, "qdc0-%02d", channel);
    sprintf(title, "charge; Charge on QDC0 channel %d];# events", channel);
    m_QDC.push_back(new TH1I(name, title, 512, 0, 3840));
  }

}

MonitorQdc::~MonitorQdc()
{
}

bool
MonitorQdc::Process(RCDRawEvent& ev)
{

  for(auto qdcData : ev.v792_data) {   // one module
    int channel = 0;
    for(auto qdcChannel : qdcData.Channel) {
      m_QDC[channel]->Fill(qdcChannel.Data);
      ++channel;
    }
  }

 return true;
}

void
MonitorQdc::Print(RCDRawEvent& ev)
{
  cout << std::dec << "QDC Modules: " << ev.v792_data.size() << "\n";
  for(auto qdcData : ev.v792_data) {
    int channel = 0;
    for(auto qdcChannel : qdcData.Channel) {
        cout << "Ch: " << channel
             << ", Value: " << qdcChannel.Data
             << endl;
      ++channel;
    }
  }

}

void
MonitorQdc::WriteToFile() {

  m_outputFile->Write();
  m_outputFile->Close();
}

void
MonitorQdc::PublishHists() {

  int channel = 0;
  for(auto qdcHists : m_QDC) {
    m_histProvider->publish(*m_QDC[channel],m_QDC[channel]->GetName());
    ++channel;
  }
  
}

void MonitorQdc::PrepareRootFile(std::string runNumberStr) {

// Store histograms in root file mirroring the OH hierarchy
// construct the ROOT file name here in 'user' space
// BEFORE booking histograms

  TDirectory * histogramming_dir = 0;
  TDirectory * monitor_dir = 0;

  const char * histogramming = "Histogramming";
  const char * monitor = "Qdc_monitor";
  string output_name;

  output_name.append("/afs/cern.ch/work/d/daquser/public/bl4smonitor/");
  output_name.append(runNumberStr);
  output_name.append(monitor);
  output_name.append(".root");
  printf(" MonitorChamber::prepareRootFile, Output file name: %s\n", output_name.c_str());
  m_outputFile = new TFile(output_name.c_str(), "RECREATE");
// give Tfile a structure
  histogramming_dir = m_outputFile->mkdir(histogramming);
  histogramming_dir->cd();
  monitor_dir = histogramming_dir->mkdir(monitor);
  monitor_dir->cd();    // current directory

}

