#include <TH1.h>
#include <TFile.h>

#include <oh/OHRootProvider.h>

#include "MonitorTDC.h"
#include "BL4SConfig.h"
#include "CommonLibrary/RCDRawEvent.h"


using namespace std;

//************************** TDC Module number hardwired   *********
  const int TDCNo = 0;
  const int V1290_CHANNELS = 16;
//******************************************************************

MonitorTdc::MonitorTdc(std::string name, BL4SConfig * cfg, 
                             std::string runNumberStr, bool publish )
{
  m_MonitorName = name;

  // BEFORE booking histograms
  if (!publish) {
    PrepareRootFile(runNumberStr);
  }

/********booking histograms**************/
/* copied from last year ...  */

  for (int channel=0; channel<V1290_CHANNELS; channel++) {
    char name[100], title[200];
    sprintf(name, "TDC%01d-%02d", TDCNo, channel);
    sprintf(title, "Timing of TDC%d channel %d Leading Edge;time [25 ps];# events", TDCNo, channel);
    m_TDC_time.push_back(new TH1I(name, title, 500, 0, 24000));
  }

  for (int channel=0; channel<V1290_CHANNELS; channel++) {
    char name[100], title[200];
    sprintf(name, "TDC%01dNcounts-%02d", TDCNo, channel);
    sprintf(title, "Number of counts of TDC%d channel %d;Ncounts;# events", TDCNo, channel);
    m_TDC_Ncounts.push_back(new TH1I(name, title, 10, 0, 10));
  }

}

MonitorTdc::~MonitorTdc()
{
}

bool
MonitorTdc::Process(RCDRawEvent& ev)
{

  for(auto tdcData : ev.v1290_data) {   // one module
    int channel = 0;
    for(auto tdcChannel : tdcData.Channel) {
      int Nc = tdcChannel.Ncounts;
      m_TDC_Ncounts[channel]->Fill(Nc);
      if (Nc > 0) {
         m_TDC_time[channel]->Fill(tdcChannel.Data);      // leading edge
      }
      ++channel;
    }
  }

 return true;
}

void
MonitorTdc::Print(RCDRawEvent& ev)
{
  cout << std::dec << "TDC Modules: " << ev.v1290_data.size() << "\n";
  for(auto tdcData : ev.v1290_data) {
    int channel = 0;
    for(auto tdcChannel : tdcData.Channel) {
        cout << "Ch: " << channel
             << ", Counts: " << tdcChannel.Ncounts
             << ", Value: " << tdcChannel.Data
             << endl;
      ++channel;
    }
  }

}

void
MonitorTdc::WriteToFile() {

  m_outputFile->Write();
  m_outputFile->Close();
}

void
MonitorTdc::PublishHists() {

  int channel = 0;
  for(auto tdcHists : m_TDC_Ncounts) {
    m_histProvider->publish(*m_TDC_time[channel],m_TDC_time[channel]->GetName());
    m_histProvider->publish(*m_TDC_Ncounts[channel],m_TDC_Ncounts[channel]->GetName());
    ++channel;
  }
  
}

void MonitorTdc::PrepareRootFile(std::string runNumberStr) {

// Store histograms in root file mirroring the OH hierarchy
// construct the ROOT file name here in 'user' space
// BEFORE booking histograms

  TDirectory * histogramming_dir = 0;
  TDirectory * monitor_dir = 0;

  const char * histogramming = "Histogramming";
  const char * monitor = "Tdc_monitor";
  string output_name;

  output_name.append("/afs/cern.ch/work/d/daquser/public/bl4smonitor/");
  output_name.append(runNumberStr);
  output_name.append(monitor);
  output_name.append(".root");
  printf(" MonitorChamber::prepareRootFile, Output file name: %s\n", output_name.c_str());
  m_outputFile = new TFile(output_name.c_str(), "RECREATE");
// give Tfile a structure
  histogramming_dir = m_outputFile->mkdir(histogramming);
  histogramming_dir->cd();
  monitor_dir = histogramming_dir->mkdir(monitor);
  monitor_dir->cd();    // current directory

}

