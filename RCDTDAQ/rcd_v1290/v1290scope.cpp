/************************************************************************/
/*									*/
/* This is a simple prototyping program for the CAEN V1290		*/
/*									*/
/* Date:   18 September 2009						*/
/* Author: Markus Joos & Jorgen Petersen				*/
/*									*/
/**************** C 2009 - A nickel program worth a dime ****************/

#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include "rcc_error/rcc_error.h"
#include "vme_rcc/vme_rcc.h"
#include "DFDebug/DFDebug.h"
#include "ROSGetInput/get_input.h"
#include "rcd_v1290/v1290.h"

/**************/
/* Prototypes */
/**************/
void mainhelp(void);
void regdecode(void);
void configure(void);
void ronedata(void);
void revent(void);
void setTriggerMatchingMode(void);
void setContinuousStorageMode(void);
void setWindowWidth(void);
void setWindowOffset(void);
void enableSubtractionOfTriggerTime(void);
void disableSubtractionOfTriggerTime(void);


/***********/
/* Globals */
/***********/
volatile v1290_regs_t *v1290;


/*****************************/
int main(int argc, char **argv)
/*****************************/
{
  int handle;
  u_int havebase = 0, mode = 0, ret, v1290_base = 0x04000000, fun = 1;
  unsigned long value;
  char c;
  static VME_MasterMap_t master_map = {0x00100000, 0x10000, VME_A32, 0};

  while ((c = getopt(argc, argv, "db:")) != -1)
  {
    switch (c) 
    {
      case 'b':	
      {
        v1290_base  = strtol(optarg, 0, 16); 
	havebase = 1;
      }
      break;
      case 'd': mode = 1; break;                   
      default:
	printf("Invalid option %c\n", c);
	printf("Usage: %s  [options]: \n", argv[0]);
	printf("Valid options are ...\n");
	printf("-b <VME A32 base address>: The hexadecimal A32 base address of the V1290\n");
	printf("-d:                        Dump registers and exit\n");
	printf("\n");
	exit(-1);
    }
  }
  
  if (!havebase)
  {
    printf("Enter the VMEbus A32 base address of the V1290\n");
    v1290_base = gethexd(v1290_base);
  }
  master_map.vmebus_address = v1290_base;

  ret = VME_Open();
  if (ret != VME_SUCCESS)
  {
    VME_ErrorPrint(ret);
    exit(-1);
  }

  ret = VME_MasterMap(&master_map, &handle);
  if (ret != VME_SUCCESS)
  {
    VME_ErrorPrint(ret);
    exit(-1);
  }

  ret = VME_MasterMapVirtualLongAddress(handle, &value);
  if (ret != VME_SUCCESS)
  {
    VME_ErrorPrint(ret);
    exit(-1);
  }
  v1290 = (v1290_regs_t *) value;
  
  if (mode ==1)
  {
    regdecode();
  }
  else
  {
    while (fun != 0)  
    {
      printf("\n");
      printf("Select an option:\n");
      printf("   1 Help\n");  
      printf("   2 Decode registers\n");  
      printf("   3 Configure module\n");  
      printf("   4 Read one data word\n");  
      printf("   5 Read one event\n");  
      printf("   6 Set Trigger Matching Mode\n");  
      printf("   7 Set Continuous Storage Mode\n");  
      printf("   8 Set Window Width\n");  
      printf("   9 Set Window Offset\n");  
      printf("   10 Enable Subtraction of Trigger Time\n");  
      printf("   11 Disable Subtraction of Trigger Time\n");  
      printf("   0 Exit\n");
      printf("Your choice ");
      fun = getdecd(fun);
      if (fun == 1) mainhelp();
      if (fun == 2) regdecode();
      if (fun == 3) configure();
      if (fun == 4) ronedata();
      if (fun == 5) revent();
      if (fun == 6) setTriggerMatchingMode();
      if (fun == 7) setContinuousStorageMode();
      if (fun == 8) setWindowWidth();
      if (fun == 9) setWindowOffset();
      if (fun == 10) enableSubtractionOfTriggerTime();
      if (fun == 11) disableSubtractionOfTriggerTime();
    }  
  } 
   
  ret = VME_MasterUnmap(handle);
  if (ret != VME_SUCCESS)
    VME_ErrorPrint(ret);
  
  ret = VME_Close();
  if (ret != VME_SUCCESS)
    VME_ErrorPrint(ret);
  exit(0);
}


/*****************/
void mainhelp(void)
/*****************/
{
  printf("\n=========================================================================\n");
  printf("Contact markus.joos@cern.ch if you need help\n");
  printf("=========================================================================\n\n");
}


// the opcode could also be DATA!!!!!
/*****************************************/
void write_micro_controller(u_short opCode)
/*****************************************/
{
  u_int n_waits = 0;
  // wait for the WRITE_OK
  while ((v1290->micro_handshake & 0x01) == 0)
  {
    n_waits++;
    //    printf("micro_handshake write:  0x%08x\n", v1290->micro_handshake);
    //    printf("micro_handshake write:  0x%08x\n", n_waits);
  }
  v1290->micro = opCode;

}


/***********************************************/
u_short read_micro_controller(u_short /*opCode*/)
/***********************************************/
{
  u_int n_waits = 0;
  // wait for the READ_OK
  while ((v1290->micro_handshake & 0x02) == 0)
  {
    n_waits++;
    //    printf("micro_handshake read:  0x%08x\n", v1290->micro_handshake);
    //    printf("micro_handshake read:  0x%08x\n", n_waits);
  }
  return v1290->micro;
}


/******************/
void regdecode(void)
/******************/
{
  u_short value, value0, value1, value2, value3;
  u_int data;

  printf("\n=========================================================================\n");
  printf("output_buffer     is at 0x%016lx\n", (u_long)&v1290->output_buffer     - (u_long)&v1290->output_buffer);
  printf("interrupt_level   is at 0x%016lx\n", (u_long)&v1290->interrupt_level   - (u_long)&v1290->output_buffer);
  printf("test_reg          is at 0x%016lx\n", (u_long)&v1290->test_reg          - (u_long)&v1290->output_buffer);
  printf("event_fifo        is at 0x%016lx\n", (u_long)&v1290->event_fifo        - (u_long)&v1290->output_buffer);
  printf("event_fifo_status is at 0x%016lx\n", (u_long)&v1290->event_fifo_status - (u_long)&v1290->output_buffer);
  printf("dummy32           is at 0x%016lx\n", (u_long)&v1290->dummy32           - (u_long)&v1290->output_buffer);
  printf("conf_rom_checksum is at 0x%016lx\n", (u_long)&v1290->conf_rom_checksum - (u_long)&v1290->output_buffer);
  printf("sernum0           is at 0x%016lx\n", (u_long)&v1290->sernum0           - (u_long)&v1290->output_buffer);

  printf("ROM data:\n");
  value0 = v1290->oui0;  value0 &= 0xff;
  value1 = v1290->oui1;  value1 &= 0xff;
  value2 = v1290->oui2;  value2 &= 0xff;
  data = (value2 << 16) | (value1 << 8) | value0;
  printf("Manufacturer identifier:     %d (0x%08x)\n", data, data);
  
  value = v1290->vers;
  printf("Board version:               %d (0x%02x)\n", value, value);

  value0 = v1290->board0;  value0 &= 0xff;
  value1 = v1290->board1;  value1 &= 0xff;
  value2 = v1290->board2;  value2 &= 0xff;
  data = (value2 << 16) | (value1 << 8) | value0;
  printf("Board ID:                    %d (0x%08x)\n", data, data); 
  
  value0 = v1290->revis0;  value0 &= 0xff;
  value1 = v1290->revis1;  value1 &= 0xff;
  value2 = v1290->revis2;  value2 &= 0xff;
  value3 = v1290->revis3;  value3 &= 0xff;
  data = (value3 << 24) | (value2 << 16) | (value1 << 8) | value0;
  printf("Board revision:              %d (0x%02x)\n", data, data);
  
  value0 = v1290->sernum0;  value0 &= 0xff;
  value1 = v1290->sernum1;  value1 &= 0xff;
  data = (value1 << 8) | value0;
  printf("Board Serial number:         %d (0x%08x)\n", data, data); 

  value0 = v1290->control_register; 
  printf("Control Register:                  %d (0x%04x)\n", value0, value0);
  printf("\nDecoding the Control Register \n");
  printf("BERR EN:                           %s\n", (value0 & 0x001)?"Yes":"No");
  printf("TERM:                              %s\n", (value0 & 0x002)?"Yes":"No");
  printf("TERM SW:                           %s\n", (value0 & 0x004)?"Yes":"No");
  printf("EMPTY EVENT:                       %s\n", (value0 & 0x008)?"Yes":"No");
  printf("ALIGN64:                           %s\n", (value0 & 0x010)?"Yes":"No");
  printf("COMPENSATION ENABLE:               %s\n", (value0 & 0x020)?"Yes":"No");
  printf("TEST FIFO ENABLE:                  %s\n", (value0 & 0x040)?"Yes":"No");
  printf("READ COMPENSATION SRAM ENABLE:     %s\n", (value0 & 0x080)?"Yes":"No");
  printf("EVENT FIFO ENABLE:                 %s\n", (value0 & 0x0100)?"Yes":"No");
  printf("EXTENDED TRIGGER TIME TAG ENABLE:  %s\n", (value0 & 0x0200)?"Yes":"No");

  value0 = v1290->status_register; 
  printf("Status Register:             %d (0x%04x)\n", value0, value0);
  printf("\nDecoding the Status Register \n");
  printf("DREADY:                            %s\n", (value0 & 0x001)?"Yes":"No");
  printf("ALMOST FULL:                       %s\n", (value0 & 0x002)?"Yes":"No");
  printf("FULL  :                            %s\n", (value0 & 0x004)?"Yes":"No");
  printf("TRG MATCH:                         %s\n", (value0 & 0x008)?"Yes":"No");
  printf("HEADER EN:                         %s\n", (value0 & 0x010)?"Yes":"No");
  printf("TERM ON:                           %s\n", (value0 & 0x020)?"Yes":"No");
  printf("ERROR0:                            %s\n", (value0 & 0x040)?"Yes":"No");
  printf("ERROR1:                            %s\n", (value0 & 0x080)?"Yes":"No");
  printf("ERROR2:                            %s\n", (value0 & 0x0100)?"Yes":"No");
  printf("ERROR3:                            %s\n", (value0 & 0x0200)?"Yes":"No");
  printf("BERR FLAG:                         %s\n", (value0 & 0x0400)?"Yes":"No");
  printf("PURGED:                            %s\n", (value0 & 0x0800)?"Yes":"No");
  value1 = value0 & 0x3000;
  printf("RES0:                              0x%04x\n",value0 & 0x01000);
  printf("RES1:                              0x%04x\n", value0 & 0x02000);
  if (value1 == 0x3000)
    printf(" Time resolution = 25 ps\n");
  if (value1 == 0x0)
    printf(" Time resolution = 800 ps\n");
  if (value1 == 0x1000)
    printf(" Time resolution = 200 ps\n");
  if (value1 == 0x2000)
    printf(" Time resolution = 100 ps\n");
  printf("PAIR MODE:                         %s\n", (value0 & 0x04000)?"Yes":"No");
  printf("TRIGGERS LOST:                     %s\n", (value0 & 0x08000)?"Yes":"No");

  value0 = v1290->geo_address; 
  printf("Geographical Address:        %d (0x%04x)\n", value0, value0);

  value0 = v1290->mcst_base_address; 
  printf("mcst_base_address:           %d (0x%04x)\n", value0, value0);

  value0 = v1290->firmware_revision; 
  printf("firmware_revision:           %d (0x%04x)\n", value0, value0);

  data = v1290->event_counter;
  printf("event_counter:               %d (0x%08x)\n", data, data);


  printf("=========================================================================\n\n");

  // the Micro Controller & HPTDC
  printf(" Micro Controller:HPTDC Parameters \n");

  write_micro_controller(READ_ACQ_MOD);
  value = read_micro_controller(READ_ACQ_MOD);
  printf("Read Acquisition Mode:             0x%08x\n", value);
  printf("TRG MATCH:                         %s\n", (value == 0x001)?"Yes":"No");
  printf("CONTINUOUS MODE:                   %s\n", (value == 0x000)?"Yes":"No");

  write_micro_controller(READ_TRG_CONF);
  value = read_micro_controller(READ_TRG_CONF);
  printf("Match Window Width:                0x%08x\n", value);
  printf("Match Window Width in ns:         %8d ns\n", 25*value);
  value = read_micro_controller(READ_TRG_CONF);
  value &= 0xfff;
  printf("Window Offset:                     0x%08x\n", value);
  value = (~value & 0xfff) +1;					// negative on 12 bits!!
  printf("Window Offset in ns(negative):   -%8d ns\n", value*25);
  value = read_micro_controller(READ_TRG_CONF);
  printf("Extra Search Window Width:         0x%08x\n", value);
  value = read_micro_controller(READ_TRG_CONF);
  printf("Reject Margin:                     0x%08x\n", value);
  value = read_micro_controller(READ_TRG_CONF);
  printf("Trigger Time Subtraction Enabled:  %s\n", (value & 0x01)?"Yes":"No");
  
  write_micro_controller(READ_DETECTION);
  value = read_micro_controller(READ_DETECTION);
  printf("Edge Detection Configuration:      0x%08x\n", value);
  printf("Trailing edge               :      %s\n", (value == 0x001)?"Yes":"No");
  printf("Leading  edge               :      %s\n", (value == 0x002)?"Yes":"No");
  
  write_micro_controller(READ_RES);
  value = read_micro_controller(READ_RES);
  printf("TDC Resolution:                    0x%08x\n", value);
  if (value == 0) 
    printf(" Resolution = 800 ps\n");
  if (value == 1) 
    printf(" Resolution = 200 ps\n");
  if (value == 2) 
    printf(" Resolution = 100 ps\n");
  if (value == 3) 
    printf(" Resolution = 25 ps\n");
  
  write_micro_controller(READ_MICRO_REV);
  value = read_micro_controller(READ_MICRO_REV);
  printf("Firmware Revision of Microcontroller: 0x%08x\n", value);
  
}


/******************/
void configure(void)
/******************/
{
  u_short width;
  u_short offset;

  v1290->software_event_reset = 0;              // SW reset: resets TDC settings

  write_micro_controller(TRG_MATCH);            // trigger matching mode

  printf("Enter the Window Width in ns\n");
  width = getdecd(500); width = width/25;	// 25 ns units
  printf(" Window Width register value to be written: 0x%08x\n", width);

  write_micro_controller(SET_WIN_WIDTH);
  write_micro_controller(width);

  printf("Enter the Window Offset in ns\n");
  offset = getdecd(400); offset = offset/25;	// 25 ns units
  offset -=1; offset = ~offset; offset &= 0xfff;// 12 bit negative
  printf(" Window Offset register value to be written: 0x%08x\n", offset);

  write_micro_controller(SET_WIN_OFFS);
  write_micro_controller(offset);
  write_micro_controller(EN_SUB_TRG);           // enable subtraction of trigger time
}


/*****************/
void ronedata(void)
/*****************/
{
  u_int wtype, vme32;
  u_int nwaits;

  printf("\n=========================================================================\n");

  printf(" status register = 0x%8x\n",v1290->status_register);

  while ((v1290->status_register & 0x01) == 0)	// wait for DREADY
  {
    printf(" DREADY = 0: no data\n");
    nwaits++;
  }

  vme32 = v1290->output_buffer;
  printf("Raw data = 0x%08x\n", vme32);
  wtype = (vme32 & 0xf8000000) >> 27;

  printf("wtype = %d\n", wtype);

  if (wtype == 0x8)		// Global Header
  {
    printf(" This is a Global Header\n");
    printf(" Event Count    = %d\n", (vme32 & 0x7ffffe0) >> 5);
    printf(" GEO            = %d\n", vme32 & 0x1f);
  }
  else if (wtype == 1)		// TDC header
  {
    printf("This is a TDC header\n");
    printf(" TDC = %d\n",(vme32 & 0x3ffffff) >> 24);
    printf(" Event ID = %d\n",(vme32 & 0xfff000) >> 12);
    printf(" Bunch ID = %d\n",(vme32 & 0xfff));
  }
  else if (wtype == 0)		// TDC measurement
  {
    printf("This is a TDC measurement\n");
    printf("Trailing measurement:  %s\n", (vme32 & 0x4000000)?"Yes":"No");
    printf("Leading measurement:  %s\n", (vme32 & 0x4000000)?"No":"Yes");
    printf(" Channel = %d\n",(vme32 & 0x3e00000) >> 21);
    printf(" TDC measurement = %d\n",(vme32 & 0x1fffff));
    printf(" TDC measurement in ps = %d\n",25*(vme32 & 0x1fffff));
  }
  else if (wtype == 3)		// TDC trailer
  {
    printf("This is a TDC trailer\n");
    printf(" TDC = %d\n",(vme32 & 0x3ffffff) >> 24);
    printf(" Event ID = %d\n",(vme32 & 0xfff000) >> 12);
    printf(" Word Count = %d\n",(vme32 & 0xfff));
  }
  else if (wtype == 4)		// TDC error
  {
    printf("This is a TDC error\n");
    printf(" TDC = %d\n",(vme32 & 0x3ffffff) >> 24);
    printf(" Error Flags = 0x%8x\n",(vme32 & 0x7ff));
  }
  else if (wtype == 0x10)		// Global Trailer
  {
    printf("This is global trailer\n");
    printf(" Word Count     = %d\n", (vme32 & 0x1fffe0) >> 5);
    printf(" GEO            = %d\n", vme32 & 0x1f);
    printf(" TDC error             :  %s\n", (vme32 & 0x4000000)?"Yes":"No");
    printf(" Output Buffer Overflow:  %s\n", (vme32 & 0x2000000)?"Yes":"No");
    printf(" Trigger Lost          :  %s\n", (vme32 & 0x1000000)?"Yes":"No");
  }
  else if (wtype == 0x11)		// Global Trigger Time Flag
  {
    printf("This is global trigger time flag\n");
  }

  printf("=========================================================================\n\n");
}


/***************/
void revent(void)
/***************/
{
  u_int wtype, vme32;
  u_int nwaits;

  while ((v1290->status_register & 0x01) == 0)	// wait for DREADY
    nwaits++;

  vme32 = v1290->output_buffer;
  wtype = (vme32 & 0xf8000000) >> 27;
  if (wtype == 0x8)			// Global Header
  {
    printf(" Global Header\n");
    printf("   Event Count    = %d\n", (vme32 & 0x7ffffe0) >> 5);
    printf("   GEO            = %d\n", vme32 & 0x1f);

    vme32 = v1290->output_buffer;		// First HPTDC
    wtype = (vme32 & 0xf8000000) >> 27;
    if (wtype == 1) 		// TDC header
    {
      printf(" TDC header\n");
      printf("   HPTDC    = %d\n",(vme32 & 0x3ffffff) >> 24);
      printf("   Event ID = %d\n",(vme32 & 0xfff000) >> 12);
      printf("   Bunch ID = %d\n",(vme32 & 0xfff));
    }
    else 
    {
      printf(" Not a TDC header - quit\n");
      return;
    }

    while(1) 
    {				// TDC measurements
      vme32 = v1290->output_buffer;
      wtype = (vme32 & 0xf8000000) >> 27;
      if (wtype == 0)		// TDC measurement
      {
        printf(" TDC measurement\n");
        printf("   Channel               = %d\n",(vme32 & 0x3e00000) >> 21);
        printf("   TDC measurement       = %d\n",(vme32 & 0x1fffff));
        printf("   TDC measurement in ps = %d\n",25*(vme32 & 0x1fffff));
        printf("   Trailing measurement:  %s\n", (vme32 & 0x4000000)?"Yes":"No");
        printf("   Leading measurement:   %s\n", (vme32 & 0x4000000)?"No":"Yes");
      }
      else if (wtype == 3)      // TDC trailer
      {
        printf(" TDC trailer\n");
        printf("   HPTDC      = %d\n",(vme32 & 0x3ffffff) >> 24);
        printf("   Event ID   = %d\n",(vme32 & 0xfff000) >> 12);
        printf("   Word Count = %d\n",(vme32 & 0xfff));
        break;		        // end of TDC block
      } 
      else
      {
        printf(" Error: NOT a TDC trailer - quit\n");
        return;
      }
    }
 
    vme32 = v1290->output_buffer;  // Second HPTDC
    wtype = (vme32 & 0xf8000000) >> 27;
    if (wtype == 1) 
    {
      printf(" TDC header\n");
      printf("   HPTDC    = %d\n",(vme32 & 0x3ffffff) >> 24);
      printf("   Event ID = %d\n",(vme32 & 0xfff000) >> 12);
      printf("   Bunch ID = %d\n",(vme32 & 0xfff));
    }
    else 
    {
      printf(" Not a TDC header - quit\n");
      return;
    }

    while(1) 
    {				// TDC measurements
      vme32 = v1290->output_buffer;
      wtype = (vme32 & 0xf8000000) >> 27;
      if (wtype == 0)		// TDC measurement
      {
        printf(" TDC measurement\n");
        printf("   Channel               = %d\n",(vme32 & 0x3e00000) >> 21);
        printf("   TDC measurement       = %d\n",(vme32 & 0x1fffff));
        printf("   TDC measurement in ps = %d\n",25*(vme32 & 0x1fffff));
        printf("   Trailing measurement:  %s\n", (vme32 & 0x4000000)?"Yes":"No");
        printf("   Leading measurement:   %s\n", (vme32 & 0x4000000)?"No":"Yes");
      }
      else if (wtype == 3)         // TDC trailer
      {
        printf(" TDC trailer\n");
        printf("   HPTDC      = %d\n",(vme32 & 0x3ffffff) >> 24);
        printf("   Event ID   = %d\n",(vme32 & 0xfff000) >> 12);
        printf("   Word Count = %d\n",(vme32 & 0xfff));
        break;		// end of TDC block
      }
      else
      {
        printf(" Error: NOT a TDC trailer - quit\n");
        return;
      }
    }

    vme32 = v1290->output_buffer;		// global trailer
    wtype = (vme32 & 0xf8000000) >> 27;
    if (wtype == 0x10)		// Global Trailer
    {
      printf(" Global trailer\n");
      printf("   Word Count             = %d\n", (vme32 & 0x1fffe0) >> 5);
      printf("   GEO                    = %d\n", vme32 & 0x1f);
      printf("   TDC error             :  %s\n", (vme32 & 0x4000000)?"Yes":"No");
      printf("   Output Buffer Overflow:  %s\n", (vme32 & 0x2000000)?"Yes":"No");
      printf("   Trigger Lost          :  %s\n", (vme32 & 0x1000000)?"Yes":"No");
    }
    else
    {
      printf(" Error: NOT a Global trailer - quit\n");
      return;
    }
  }
  else
    printf(" First word NOT a Global Header - Quit .. \n");

  return;
}


/***************/
void dumpth(void)
/***************/
{
}

/*******************************/
void setTriggerMatchingMode(void)
/*******************************/
{
  write_micro_controller(TRG_MATCH);
}


/*********************************/
void setContinuousStorageMode(void)
/*********************************/
{
  write_micro_controller(CONT_STOR);
}


/***********************/
void setWindowWidth(void)
/***********************/
{
  u_short width;
  printf("Enter the Window Width in ns\n");
  width = getdecd(1000); width = width/25;		// 25 ns units
  printf(" Window Width register value to be written: 0x%08x\n", width);

  write_micro_controller(SET_WIN_WIDTH);
  write_micro_controller(width);
}


/************************/
void setWindowOffset(void)
/************************/
{
  u_short offset;
  printf("Enter the Window Offset in ns\n");
  offset = getdecd(1000); offset = offset/25;		// 25 ns units
  offset -=1; offset = ~offset; offset &= 0xfff;	// 12 bit negative
  printf(" Window Offset register value to be written: 0x%08x\n", offset);

  write_micro_controller(SET_WIN_OFFS);
  write_micro_controller(offset);
}


/***************************************/
void enableSubtractionOfTriggerTime(void)
/***************************************/
{
  write_micro_controller(EN_SUB_TRG);
}


/****************************************/
void disableSubtractionOfTriggerTime(void)
/****************************************/
{
  write_micro_controller(DIS_SUB_TRG);
}
