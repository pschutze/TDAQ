/********************************************************/
/*							*/
/* Date: 07 October 2009				*/ 
/* Author: Markus Joos& Jorgen Petersen			*/
/*							*/
/*** C 2009 - The software with that certain something **/

#ifndef DATACHANNELV1290_H
#define DATACHANNELV1290_H

#include "ROSCore/SingleFragmentDataChannel.h"


namespace ROS 
{
  class DataChannelV1290 : public SingleFragmentDataChannel 
  {
  public:    
    DataChannelV1290(u_int id, u_int configId, u_int rolPhysicalAddress, u_long vmeBaseVA, DFCountedPointer<Config> configuration);    
    virtual ~DataChannelV1290() ;
    virtual int getNextFragment(u_int* buffer, int max_size, u_int* status, unsigned long pciAddress = 0);
    virtual DFCountedPointer < Config > getInfo();

  private:
    v1290_regs_t *m_v1290;
    u_int m_max_wait;
    u_int m_min_wait;
    u_int m_channel_number;    
    u_int m_channelId;
    u_int m_model;
    u_int m_version;
    u_int m_nHPTDC;
    
    enum Statuswords 
    {
      S_OK = 0,
      S_TIMEOUT,
      S_OVERFLOW,
      S_NODATA
    };
  };
}
#endif //DATACHANNELV1290_H
