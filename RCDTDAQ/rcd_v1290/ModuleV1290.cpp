/********************************************************/
/*							*/
/* Date: 07 October 2009				*/ 
/* Author: Markus Joos & Jorgen Petersen		*/
/*							*/
/*** C 2009 - The software with that certain something **/

#include <unistd.h>
#include <iostream>
#include "rcc_error/rcc_error.h"
#include "vme_rcc/vme_rcc.h"
#include "DFDebug/DFDebug.h"
#include "ROSUtilities/ROSErrorReporting.h"
#include "ROSModules/ModulesException.h"
#include "rcd_v1290/v1290.h"
#include "rcd_v1290/DataChannelV1290.h"
#include "rcd_v1290/ModuleV1290.h"
#include "rcd_v1290/ExceptionV1290.h"

using namespace ROS;
using namespace RCD;

/************************/
ModuleV1290::ModuleV1290() 
/************************/
{ 
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "ModuleV1290::constructor: Entered");
}


/************************************************/
ModuleV1290::~ModuleV1290()   noexcept
/************************************************/
{
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "ModuleV1290::destructor: Entered");

  while (m_dataChannels.size() > 0) 
  {
    DataChannel *channel = m_dataChannels.back();
    m_dataChannels.pop_back();
    delete channel;		// we created them
  }
}


/*************************************************************/
void ModuleV1290::setup(DFCountedPointer<Config> configuration) 
/*************************************************************/
{ 
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "ModuleV1290::setup: Entered");

  m_configuration = configuration;

  //get VMEbus parameters for the V1290 MODULE
  m_vmeAddress = configuration->getInt("VMEbusAddress");
  m_vmeWindowSize = configuration->getInt("VMEbusWindowSize");
  m_vmeAddressModifier = configuration->getInt("VMEbusAddressModifier");
  m_id = configuration->getInt("channelId", 0);
  m_rolPhysicalAddress = configuration->getInt("ROLPhysicalAddress", 0);

  m_windowWidth = configuration->getInt("WindowWidth");
  m_windowOffset = configuration->getInt("WindowOffset");
 
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 20, "ModuleV1290::setup: id = " << m_id << " rolPhysicalAddress = "
             << m_rolPhysicalAddress);
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 20, "ModuleV1290::setup: vmeAddress = " << HEX(m_vmeAddress)
             << "  vmeWindowSize  = " << HEX(m_vmeWindowSize) << "  vmeAddressModifier = "
             << HEX(m_vmeAddressModifier));
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 20, "ModuleV1290::setup: window width  = " << m_windowWidth
             << "  window offset  = " << m_windowOffset );
}


/************************************************************/
void ModuleV1290::configure(const daq::rc::TransitionCmd&)
/************************************************************/
{
  err_type ret;
  err_str rcc_err_str;
  u_short width;
  u_short offset;

  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "ModuleV1290::configure: Entered");
  ret = VME_Open();
  if (ret != VME_SUCCESS) 
  {
    DEBUG_TEXT(DFDB_RCDEXAMPLE, 5, "ModuleV1290::configure: Error from VME_Open");
    rcc_error_string(rcc_err_str, ret);
    CREATE_ROS_EXCEPTION(ex1, ModulesException, VME_OPEN, rcc_err_str);
    throw (ex1);
  }

  //Create the DataChannel object
  VME_MasterMap_t master_map;
  master_map.vmebus_address   = m_vmeAddress;
  master_map.window_size      = m_vmeWindowSize;
  master_map.address_modifier = m_vmeAddressModifier;
  master_map.options          = 0;

  ret = VME_MasterMap(&master_map, &m_vmeScHandle);
  if (ret != VME_SUCCESS) 
  {
    DEBUG_TEXT(DFDB_RCDEXAMPLE, 5, "ModuleV1290::configure: Error from VME_MasterMap");
    rcc_error_string(rcc_err_str, ret);
    CREATE_ROS_EXCEPTION(ex2, ModulesException, VME_MASTERMAP, rcc_err_str);
    throw (ex2);
  }
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 20, "ModuleV1290::configure: VME_MasterMap, handle = " << m_vmeScHandle);

  ret = VME_MasterMapVirtualLongAddress(m_vmeScHandle, &m_vmeVirtualPtr);
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 20, "ModuleV1290::configure: VME_MasterMapVirtualAddress, VA = " << HEX(m_vmeVirtualPtr));
  m_v1290 = reinterpret_cast<v1290_regs_t*>(m_vmeVirtualPtr);  // (virtual) base address of the channel

  DataChannelV1290 *channel = new DataChannelV1290(m_id, 0, m_rolPhysicalAddress, m_vmeVirtualPtr, m_configuration);
  m_dataChannels.push_back(channel);
  
  //Initialize the V1290 card

  m_v1290->software_event_reset = 0;		        // SW reset: resets TDC settings

  write_micro_controller(TRG_MATCH);		        // trigger matching mode

  width = m_windowWidth/25;                             // 25 ns units
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 20, "ModuleV1290::configure: window width to be written = " << HEX(width));
  write_micro_controller(SET_WIN_WIDTH);	        // window width
  write_micro_controller(width);

  offset = m_windowOffset/25;                           // 25 ns units
  offset -=1; offset = ~offset; offset &= 0xfff;        // 12 bit negative
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 20, "ModuleV1290::configure: window offset to be written = " << HEX(offset));
  write_micro_controller(SET_WIN_OFFS);		        // window offset
  write_micro_controller(offset);

  write_micro_controller(EN_SUB_TRG);		        // enable subtraction of trigger time

  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "ModuleV1290::configure: Done");
}


/***********************************************************/
void ModuleV1290::unconfigure(const daq::rc::TransitionCmd&)
/***********************************************************/
{
  err_type ret;
  err_str rcc_err_str;

  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "ModuleV1290::unconfigure: Entered");

  ret = VME_MasterUnmap(m_vmeScHandle);
  if (ret != VME_SUCCESS) 
  {
    DEBUG_TEXT(DFDB_RCDEXAMPLE, 5, "ModuleV1290::unconfigure: Error from VME_MasterUnmap");
    rcc_error_string(rcc_err_str, ret);
    CREATE_ROS_EXCEPTION(ex3, ModulesException, VME_MASTERUNMAP, rcc_err_str);
    throw (ex3);
  }

  ret = VME_Close();
  if (ret != VME_SUCCESS) 
  {
    DEBUG_TEXT(DFDB_RCDEXAMPLE, 5, "ModuleV1290::unconfigure: Error from VME_Close");
    rcc_error_string(rcc_err_str, ret);
    CREATE_ROS_EXCEPTION(ex4, ModulesException, VME_CLOSE, rcc_err_str);
    throw (ex4);
  }
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "ModuleV1290::unconfigure: Done");
}


/************************************************************/
void ModuleV1290::connect(const daq::rc::TransitionCmd&)
/************************************************************/
{
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 20, "ModuleV1290::connect: Entered");
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "ModuleV1290::connect: Done");
}


/***************************************************************/
void ModuleV1290::disconnect(const daq::rc::TransitionCmd&)
/***************************************************************/
{
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "ModuleV1290::disconnect: Entered");
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "ModuleV1290::disconnect: Done");
}
    

/****************************************************************/    
void ModuleV1290::prepareForRun(const daq::rc::TransitionCmd&)
/****************************************************************/    
{
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "ModuleV1290::prepareForRun: Entered");

  m_v1290->software_clear = 0;		// TDCs cleared, Output Buffer cleared, Event Counter cleared

  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "ModuleV1290::prepareForRun: SW Clear done");
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "ModuleV1290::prepareForRun: Done");
}


/************************************************************/
void ModuleV1290::stopDC(const daq::rc::TransitionCmd&)
/************************************************************/
{
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "ModuleV1290::stopDC: Entered");
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "ModuleV1290::stopDC: Done");
}


/************************************************/
DFCountedPointer < Config > ModuleV1290::getInfo()
/************************************************/
{
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "ModuleV1290::getInfo: Entered");
  DFCountedPointer<Config> info = Config::New();

  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "ModuleV1290::getInfo: Done");
  return(info);
}


/***************************/
void ModuleV1290::clearInfo()
/***************************/
{
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "ModuleV1290::clearInfo: Entered");
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "ModuleV1290::clearInfo: Done");
}


// these two functions could go into a library ...
// the opcode could also be DATA!!!!!
/******************************************************/
void ModuleV1290::write_micro_controller(u_short opCode)
/******************************************************/
{
  u_int n_waits = 0;
  // wait for the WRITE_OK
  while ((m_v1290->micro_handshake & 0x01) == 0)
  {
    n_waits++;
    //  printf("micro_handshake write:  0x%08x\n", m_v1290->micro_handshake);
    //  printf("micro_handshake write:  0x%08x\n", n_waits);
  }
  m_v1290->micro = opCode;
}


/********************************************************/
u_short ModuleV1290::read_micro_controller(u_short opCode)
/********************************************************/
{
  u_int n_waits = 0;
  // wait for the READ_OK
  while ((m_v1290->micro_handshake & 0x02) == 0)
  {
    n_waits++;
    //  printf("micro_handshake read:  0x%08x\n", m_v1290->micro_handshake);
    //  printf("micro_handshake read:  0x%08x\n", n_waits);
  }
  return m_v1290->micro;
}


//FOR THE PLUGIN FACTORY
extern "C" 
{
  extern ReadoutModule* createReadoutModuleV1290();
}
ReadoutModule* createReadoutModuleV1290()
{
  return (new ModuleV1290());
}
