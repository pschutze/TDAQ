#ifndef V785EXCEPTION_H
#define V785EXCEPTION_H

#include "DFExceptions/ROSException.h"
#include <string>
#include <iostream>

namespace RCD 
{
  using namespace ROS;

  class V785Exception : public ROSException 
  {
  public:
    enum ErrorCode 
    {
      USERWARNING = 1,
      V785_DUMMY2
    };
    V785Exception(ErrorCode errorCode);
    V785Exception(ErrorCode errorCode, std::string description);
    V785Exception(ErrorCode errorCode, const ers::Context& context);
    V785Exception(ErrorCode errorCode, std::string description, const ers::Context& context);
    V785Exception(const std::exception& cause, ErrorCode error, std::string description, const ers::Context& context);

    virtual ~V785Exception() throw() { }

  protected:
    virtual std::string getErrorString(u_int errorId) const;
  };
  
  inline std::string V785Exception::getErrorString(u_int errorId) const 
  {
    std::string rc;
    switch (errorId) 
    {
    case USERWARNING:
      rc = "Something went wrong. Read on:";
      break;
    case V785_DUMMY2:
      rc = "Dummy2";
      break;
    default:
      rc = "";
      break;
    }
    return rc;
  }
  
}
#endif //V785EXCEPTION_H
