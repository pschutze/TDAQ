# This file contains some useful functions
# for the BL4S RCDTDAQ build environment

MESSAGE(STATUS "BL4S Utilities loaded")

# Adds a target to build and install a ROSinfo header for a given readout module
FUNCTION(BL4S_ADD_ROSINFO_HEADER modname)
  ADD_CUSTOM_COMMAND(OUTPUT ${rosinfo_dir}/DataChannel${modname}Info.h
    COMMAND is_generator.sh -p ROS -pd ROSInfo -cn DataChannel${modname}Info
      -d ${CMAKE_INSTALL_PREFIX}/../include/ROSInfo -cpp ${src_dir}/${modname}Info.schema.xml
    DEPENDS ${schema}
    )

  ADD_CUSTOM_TARGET(DataChannel${modname}Info.h ALL
    DEPENDS ${rosinfo_dir}/DataChannel${modname}Info.h
    )
ENDFUNCTION(BL4S_ADD_ROSINFO_HEADER)

# Adds a git submodule (external/packagename) to the build.
# Checks if the submodule is reachable and tries to fix it by initializing and
# updating the module.
FUNCTION(BL4S_ADD_EXTERNAL_PACKAGE packageName)
  IF(NOT EXISTS $ENV{DAQ_ROOT}/RCDTDAQ/external/${packageName}/CMakeLists.txt)
    MESSAGE(STATUS "external/${packageName} not found: trying to fix that")
    EXECUTE_PROCESS(COMMAND git submodule init
                    COMMAND git submodule update
                    WORKING_DIRECTORY $ENV{DAQ_ROOT})
    IF(NOT EXISTS $ENV{DAQ_ROOT}/RCDTDAQ/external/${packageName}/CMakeLists.txt)
      MESSAGE(SEND_ERROR "external/${packageDir}: automatic fix failed. I surrender!")
      RETURN()
    ENDIF()
  ENDIF()
  MESSAGE(STATUS "Adding external package - ${packageName}")
  ADD_SUBDIRECTORY(external/${packageName})
ENDFUNCTION(BL4S_ADD_EXTERNAL_PACKAGE)

