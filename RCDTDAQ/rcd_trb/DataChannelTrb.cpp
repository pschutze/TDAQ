/********************************************************/
/*							*/
/* Date: 09/02/2017					*/ 
/* Author: J.O.Petersen					*/
/* adapt TRB code to MicroMegas FEC			*/
/*							*/
/*** C 2015 - The software with that certain something **/

#include <unistd.h>


#include <stdint.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include "ROSUtilities/ROSErrorReporting.h"
#include "DFDebug/DFDebug.h"
#include "DFSubSystemItem/Config.h"
#include "rcd_trb/ExceptionTrb.h"
#include "rcd_trb/trb.h"
#include "rcd_trb/DataChannelTrb.h"

using namespace ROS;
using namespace RCD;

/**************************************************************************/
DataChannelTRB::DataChannelTRB(u_int channelId,
				 u_int channelIndex,
				 int trbsocket,
				 DFCountedPointer<Config> configuration,
				 DataChannelTRBInfo *info) :
  SingleFragmentDataChannel(channelId, channelIndex, 0, configuration, info)
/**************************************************************************/
{ 
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "DataChannelTRB::constructor: Entered");
  m_statistics = info;
  m_channelId = channelId;
  m_socket = trbsocket;
  m_nAPVs = configuration->getInt("NumAPVs");
  m_busyDelay = configuration->getInt("BusyDelay");
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 20, "DataChannelTRB::constructor: channelId = " << m_channelId);
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 20, "DataChannelTRB::constructor: sock = " << m_socket);
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 20, "DataChannelTRB::constructor: nAPVs = " << m_nAPVs);
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 20, "DataChannelTRB::constructor: busyDelay = " << m_busyDelay);
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "DataChannelTRB::constructor: Done");
}


/*********************************/
DataChannelTRB::~DataChannelTRB() 
/*********************************/
{
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "DataChannelTRB::destructor: Entered");
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "DataChannelTRB::destructor: Done");
}


/****************************************************************************/
int DataChannelTRB::getNextFragment(u_int* buffer, int max_size,
                                    u_int* status, u_long /*pciAddress*/)
/****************************************************************************/
{
  int fsize = 0;
  socklen_t fromlen;
  struct sockaddr_in from;
  fromlen = sizeof(struct sockaddr_in);
  u_int l_status = S_OK;

  ++m_nEvents;

  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15,
             "DataChannelTRB::getNextFragment: Entered for channel = " << std::hex <<
             m_channel_number << std::dec);
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "DataChannelTRB::getNextFragment: max_size = " << max_size);

  u_int* curBufPtr = buffer;
  int curMaxSize = max_size - 4 -4;	//bytes
  u_int firstWord;

  u_int ip4;		// IP addrees without dots

// we get here after the CORBO has received a trigger
// expect a series of UDP packets. one per APV, terminated by a frame with one word: FAFAFAFA
// per FEC !!
  u_int recordedAPVs = 0;
  do {
//  leave two words
    int n_bytes = recvfrom(m_socket,
                           curBufPtr+1+1,		// IP address, # words
                           curMaxSize,
                           0,
                           (struct sockaddr *)&from,
                           &fromlen);
    DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "DataChannelTRB::getNextFragment:  # bytes read = " << n_bytes);
    
    if ((n_bytes == -1) && (errno == EAGAIN)) { // timeout
      DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "DataChannelTRB::getNextFragment: Timeout ");
      std::ostringstream msgStream;
      msgStream << "DataChannelTRB: Timeout (Event: " << m_nEvents << ", received data: "
                << recordedAPVs << " APVs)";
      std::cout << msgStream.str() << std::endl;
      ERS_REPORT_IMPL(ers::warning, ers::Message, msgStream.str(), );

      //prepare an empty event
      *curBufPtr = 0;
      fsize = 4;        // reflects previous curBufPtr++ 's

      *status = S_TIMEOUT;
      return(fsize);
    }

    if (n_bytes< 0) {
      DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "DataChannelTRB::getNextFragment: Failed to receive");
      char* strerr = strerror(errno);
      CREATE_ROS_EXCEPTION(ex1, TRBException, UDP_RECEIVE, strerr);
      throw ex1;
    }
    firstWord = curBufPtr[2];
    DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "DataChannelTRB::getNextFragment:  first word =  "
               <<  std::hex << firstWord << std::dec);

    ip4 = ntohl(from.sin_addr.s_addr);

    DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "DataChannelTRB::getNextFragment:  IP address of sender "  << std::hex << ip4 << std::dec );

    curBufPtr[0] = ip4;
    curBufPtr[1] = n_bytes/sizeof(int);

    curBufPtr += (n_bytes/sizeof(int) + 1 + 1);
    curMaxSize -= (n_bytes + 4 +4);
    DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "DataChannelTRB::getNextFragment:  curBufPtr "  << curBufPtr);
    DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "DataChannelTRB::getNextFragment:  curMaxSize "  << curMaxSize);

    fsize += (n_bytes + 4 + 4);
    ++recordedAPVs;
  } while(firstWord != 0xFAFAFAFA);

  //Check for number of APVs in the data stream
  recordedAPVs-=1; //removing trailer
  if (recordedAPVs != m_nAPVs){
    std::ostringstream msgStream;
    msgStream << "Received data for " << recordedAPVs << " APVs (expected "
              << m_nAPVs << ") at event " << m_nEvents;
    
    std::cout << msgStream.str() << std::endl;
    ERS_REPORT_IMPL(ers::warning, ers::Message, msgStream.str(), );
    l_status = S_BADAPVS;
  }
  
  *status = l_status;

  // Artificial dead time in us
  usleep(m_busyDelay);
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "DataChannelTRB::getNextFragment getNextFragment with fsize = " << fsize);
  return fsize;	// bytes ..
}


/**********************************/
ISInfo *DataChannelTRB::getISInfo()
/**********************************/
{
//  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "TRBDataChannel::getIsInfo: called");
//  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "TRBDataChannel::getIsInfo: done");
  return m_statistics;
}


/************************************/
void DataChannelTRB::clearInfo(void)
/************************************/
{
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 5, "TRBDataChannel::clearInfo: entered");
  SingleFragmentDataChannel::clearInfo();
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 5, "TRBDataChannel::clearInfo: done");
}




