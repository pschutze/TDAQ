/********************************************************/
/*							*/
/* Date:   1 November 2006				*/ 
/* Author: Markus Joos					*/
/*							*/
/*** C 2006 - The software with that certain something **/

#ifndef MODULEV792_H
#define MODULEV792_H

#include "ROSCore/ReadoutModule.h"

namespace ROS 
{
  class ModuleV792 : public ReadoutModule
  {
  public:
    virtual void setup(DFCountedPointer<Config> configuration);
    virtual void configure(const daq::rc::TransitionCmd&);
    virtual void prepareForRun(const daq::rc::TransitionCmd&);
    virtual void stopDC(const daq::rc::TransitionCmd&);
    virtual void unconfigure(const daq::rc::TransitionCmd&);
    virtual void connect(const daq::rc::TransitionCmd&);
    virtual void disconnect(const daq::rc::TransitionCmd&);
    virtual void clearInfo();
    virtual DFCountedPointer < Config > getInfo();
    ModuleV792();
    virtual ~ModuleV792()  noexcept;

    virtual const std::vector<DataChannel *> *channels();
    
  private:
    DFCountedPointer<Config> m_configuration;
    std::vector<DataChannel *> m_dataChannels;

    // channel parameters
    u_int m_id;
    u_int m_rolPhysicalAddress;

    // VMEbus parameters of a module
    u_int m_vmeAddress;
    u_int m_vmeWindowSize;
    u_int m_vmeAddressModifier;
    unsigned long m_vmeVirtualPtr;
    u_int m_iped;
    int m_vmeScHandle;
    v792_regs_t *m_v792;
  };

  inline const std::vector<DataChannel *> *ModuleV792::channels()
  {
    return &m_dataChannels;
  }  
}
#endif // MODULEV792_H
