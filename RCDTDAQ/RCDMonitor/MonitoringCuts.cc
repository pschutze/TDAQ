#include "MonitoringCuts.h"

using namespace std;

static const vector<string> variable_names= {"Sc_2","Sc_3","M_counter","Sc_timing",
                                             "Preshower_p","Preshower_e",
                                             "LeadGlass_p","LeadGlass_e",
                                             "Cherenkov1_p","Cherenkov1_e",
                                             "Cherenkov2_p","Cherenkov2_e",
                                             "DritTime_ns","P1_Straw_Ecut_keV","P2_Straw_Ecut_keV",
                                             "P1_Straw_1","P1_Straw_2","P1_Straw_3","P1_Straw_4",
                                             "P1_Straw_5","P1_Straw_6","P1_Straw_7","P1_Straw_8",
                                             "P1_Straw_9","P1_Straw_10","P1_Straw_11","P1_Straw_12",
                                             "P2_Straw_1","P2_Straw_2","P2_Straw_3","P2_Straw_4",
                                             "P2_Straw_5","P2_Straw_6","P2_Straw_7","P2_Straw_8",
                                             "P2_Straw_9","P2_Straw_10","P2_Straw_11","P2_Straw_12"
                                            };

MonitoringCuts::MonitoringCuts()
{
  fMoncutsfilePass = "null";
  fCutValues.clear();
}

MonitoringCuts::MonitoringCuts(string moncutsfilename)
{
  SetCutsFile(moncutsfilename);
  fCutValues.clear();
}


MonitoringCuts::~MonitoringCuts()
{
  fCutValues.clear();
}


void MonitoringCuts::SetCutsFile(string moncutsfilename)
{
  size_t found = moncutsfilename.find(string("/"));
  if(found==std::string::npos){
    string trt_configs_path = getenv("TRT_CONFIGS");
    fMoncutsfilePass = trt_configs_path + string("/") + moncutsfilename;
  } else {
    fMoncutsfilePass = moncutsfilename;
  }
  // cout << "MonitoringCuts::SetCutsFile():: : " << fMoncutsfilePass << "\n";
}


void MonitoringCuts::SetMonitoringCuts(string moncutsfilename)
{
  SetCutsFile(moncutsfilename);
  ParseFile();
}


void MonitoringCuts::createCut(string name, double minCut, double maxCut) 
{
  if(hasCutName(name))
  {
    printf("Attempt to create second cut named %s \n\n", name.c_str());
    abort();
  }
  fCutValues[name] = make_pair(minCut,maxCut);
}


double MonitoringCuts::getMinCut(string name)
{
  if(hasCutName(name)) {
    return fCutValues[name].first;
  } else {
    printf("\nERROR\n MonitoringCuts:: requested %s cannot be accessed \n Check cuts in the %s file!\n", name.c_str(), fMoncutsfilePass.c_str());
    printf("WARNING\n MonitoringCuts:: creating new empty cut %s (Min=-1e9 and Max=1e9) \n", name.c_str());
    createCut(name, -1e9, 1e9);
    std::cout << "--- New Monitoring Cuts Map----\n";
    ShowAllCuts();
    std::cout << "\n\n";
    // abort();
  }
}

double MonitoringCuts::getMaxCut(string name)
{
  if(hasCutName(name)) {
    return fCutValues[name].second;
  } else {
    printf("\nERROR\n MonitoringCuts:: requested %s cannot be accessed \n Check cuts in the %s file!\n", name.c_str(), fMoncutsfilePass.c_str());
    printf("WARNING\n MonitoringCuts:: creating new empty cut %s (Min=-1e9 and Max=1e9) \n", name.c_str());
    createCut(name, -1e9, 1e9);
    std::cout << "--- New Monitoring Cuts Map----\n";
    ShowAllCuts();
    std::cout << "\n\n";
    // abort();
  }
}


void MonitoringCuts::ParseFile()
{
  std::string inpline;

  ifstream CutsText (fMoncutsfilePass);
  cout << "MonitoringCuts::ParseFile():: : " << fMoncutsfilePass << "\n";
  int linenumber = 0;
  if (CutsText.is_open()) {
      std::cout << "--- Monitoring Cuts ----\n";
      while ( std::getline (CutsText,inpline) ) {
        // std::cout << inpline << '\n';
        linenumber++;
        if (inpline[0]=='/') continue;
        if( ReadLine(inpline) ){
          std::cout <<"MonitoringCuts::ParseFile():: : Line "<< linenumber << " " << fMoncutsfilePass << "  couldn't be interpereted \n";
          std::cout <<"MonitoringCuts::ParseFile():: : Line is : "<<inpline << '\n';
        } 
      }
      CutsText.close();
      // ShowAllCuts();
  } else {
    std::cout << "Unable to open Monitoring_Cuts.txt" << endl; 
  }
}


int MonitoringCuts::ReadLine( string inpline ) 
{
  std::istringstream iss(inpline);
  // std::string token;
  std::vector<std::string> tokens;
  for (std::string each; std::getline(iss, each, ','); tokens.push_back(each));

  if(tokens.size()>3) {
    std::cout << "MonitoringCuts::ReadLine():: : Too many commas in config line. \n";
    return 1;
  }

  for(int iTokens=0; iTokens < tokens.size(); iTokens++){
    // std::cout << "(" << tokens[iTokens] << ")" << std::endl;
    for(int i=0; i < variable_names.size(); i++){
      std::size_t found = tokens[iTokens].find(variable_names[i]);
      if (found!=std::string::npos){
        // cout<< "MonitoringCuts::ReadLine():: :" << variable_names[i] << " found \n";
        createCut(tokens[0], atof(tokens[1].c_str()), atof(tokens[2].c_str()));
        return 0;
      }
      // else
      //   cout << "\nERROR\n MonitoringCuts::ReadLine() Unknown cut " << tokens[iTokens] <<"! Skip it. Check cut list and variable_names.\n";
    }
  }

  return 1;
}


void MonitoringCuts::ShowAllCuts()
{  
  std::map<std::string, std::pair<double,double>>::const_iterator itr = fCutValues.begin();
  std::map<std::string, std::pair<double,double>>::const_iterator itrE = fCutValues.end();

  cout << "Cut map size = " << fCutValues.size() << "\n";
  std::cout << std::setw(18) << "Parameter:" 
            << std::setw(10) << "Min:" 
            << std::setw(10) << "Max:" << std::endl;
  for(; itr!=itrE; ++itr){
    std::pair<double, double> p1 = itr->second;
    std::cout << std::setw(18) << string(itr->first) 
              << std::setw(10) << p1.first 
              << std::setw(10) << p1.second << std::endl;
  } 
}

