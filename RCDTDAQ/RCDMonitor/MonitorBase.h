#ifndef __MONITOR_BASE_H__
#define __MONITOR_BASE_H__

#include "RCDRawEvent.h"
#include "HistogramStore.h"
#include "MonitoringCuts.h"

#include <vector>

#include <TH1F.h>
#include <TProfile.h>


class MonitorBase {
  public:
    virtual void SetMonitoringCuts(string moncutsfilename)    { fMonitoringCuts.SetMonitoringCuts(moncutsfilename); }
    virtual void ShowMonitoringCuts() { fMonitoringCuts.ShowAllCuts(); }

    virtual bool Process(RCDRawEvent& ev) = 0;
   	virtual void Print(RCDRawEvent& ev) = 0;

    std::vector<TH1*> GetHistograms() { return fHists.getListOfHistograms(); }
    std::string GetMonitorName() { return fMonitorName; }

  private:
    // virtual void ResetVars() {};

  protected:
    std::string fMonitorName;
    HistogramStore fHists;
    MonitoringCuts fMonitoringCuts;

  protected:
};


#endif
