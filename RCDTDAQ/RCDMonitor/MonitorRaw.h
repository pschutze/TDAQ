#ifndef __MONITOR_RAW__ 
#define __MONITOR_RAW__ 

#include "MonitorBase.h"
#include "HistogramStore.h"

#include <vector>


class MonitorRaw: public MonitorBase {
  public:
    MonitorRaw();
    MonitorRaw(std::string name);
    ~MonitorRaw();

   	virtual bool Process(RCDRawEvent& ev);
   	virtual void Print(RCDRawEvent& ev);

  private:
    const int fMaxTDC;
    const int fMaxQDC;

};

  
#endif
