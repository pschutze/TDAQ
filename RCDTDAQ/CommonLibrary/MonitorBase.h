#ifndef __MONITOR_BASE_H__
#define __MONITOR_BASE_H__

#include "RCDRawEvent.h"
class OHRootProvider;
//class RCDRawEvent;

class MonitorBase {
  public:
    virtual bool Process(RCDRawEvent& ev) = 0;
    virtual void Print(RCDRawEvent& ev) = 0;
    virtual void WriteToFile() = 0;

    virtual void PublishHists() = 0;
    
    void SetHistProvider(OHRootProvider* prov) { m_histProvider = prov; }

  protected:
     OHRootProvider* m_histProvider;

};

#endif
