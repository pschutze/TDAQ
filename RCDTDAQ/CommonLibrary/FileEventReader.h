#include <iostream>
#include <fstream>
#include <cstring>
#include <string>
#include <cmath>
#include <stdio.h>
#include <stdlib.h>
#include <algorithm>
#include <ctime>
#include <time.h>
#include <list>

#include "EventReaderBase.h"

/*#include "BL4SConfig.h"*/
/*#include "BL4SDebug.h"*/
/**//*#include "BL4SException.h"*/

#ifndef __FILE_EVENT_READER_H__
#define __FILE_EVENT_READER_H__ 


class FileEventReader : public EventReaderBase {
  public:
    FileEventReader(std::string fname, bool multiFileReader=true,
                    int numEvents=-1, int skipEvents=0,
                    int firstFile=1); //, BL4SConfig *cfg
    ~FileEventReader();
    time_t GetTimeStamp(){return fOpenTimestamp;}
    ///Used to read the next event. STL version
    virtual std::vector<unsigned int> GetNextRawEvent();
    
    ///Skips the event without processing it. If nodebug is set, it doesn't print any debugging information.
    //void SkipEvent(bool nodebug=false);
    void ShowFileInfo();

    static u_int GetNumEventsFromFile(std::string fname, bool total=false);
    
  private:
    bool fIsFileOpen;
    bool fMultiFile;
    bool fIsFirstFile;
    
    std::ifstream fRawFile;
    //const std::string fCurrentFileName;
    std::list<std::pair<std::string, int> > fFiles;
    std::list<int> fFileNames;
    
    u_int * fMemblock; // The memory block that contains the whole file (or single event) 
    int fMemblockSize; // Current size of the allocated event memory
    int fNwords; // Number of total 4 byte words in the file
    int fIndex; // fIndex in the memblock array

    /*BL4SDebug * debug;*/

    int fSkipEvents; // Number of events to skip
   // int fNumEvents;  
    int fReadEvents; // Number of events already processed
    int fNumberInSequence; // Number of the file in a file sequence
    int fProcessNevents;   // Number of events to process
    int fFirstFile;
    time_t fOpenTimestamp;
    time_t fCloseTimestamp;

    void ReadHeaderFooter();
    int ExtractRunNumber(std::string fname);
    time_t GetTimestampFromString(std::string datetimestring);
    //Open a new file and read header/footer
    void OpenFile(std::string fname);
};
#endif

