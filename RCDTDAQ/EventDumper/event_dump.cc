/**************************************************/
/*  Dump events online or from a datafile         */
/*                                                */
/*  author:  Oskar Wyszynski & J.Petersen         */
/**************************************************/

#include <unistd.h>
#include <signal.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/stat.h>
#include <boost/format.hpp>

#include <cmdl/cmdargs.h>
#include <ipc/core.h>

#include "DAQEventReader.h"
#include "FileEventReader.h"
#include "RCDRawEvent.h"

using namespace std;

void ProcessEvents(EventReaderBase &evReader, int nEvents,
              bool processData = false, bool noRawOutput = false);
void term_handler(int);

int 
main(int argc, char** argv)
{
  CmdArgStr partition_name ('p', "partition", "partition-name", "partition to work in." );
  CmdArgInt events         ('e', "events", "events-number", "number of events to retrieve (default 1)\n"
                                                         "-1 means work forever" );
  CmdArgStr datafilename   ('f', "datafilename", "asynchronous", "Data file name, if entered it reads events from the file instead of online stream");
  CmdArgSet multiFileRun   ('m', "multirun", "Run over all files of a run (offline) or continuously over run boundaries (online)");
  CmdArgSet processData    ('x', "process", "Process raw data through RCDRawEvent");
  CmdArgSet noRawOutput    ('n', "no-raw-output", "Don't print raw data");
  CmdArgInt skipEvents     ('s', "skip", "skip-events", "Number of events to skip (offline)");
  CmdArgInt startFile      ('S', "start-file", "start-file", "Start at number of file in sequence (offline)");
  
  partition_name = getenv("TDAQ_PARTITION");
  events = 1;
  multiFileRun = false;
  processData = false;
  noRawOutput = false;
  skipEvents = 0;
  startFile = 1;
  
  CmdArgvIter arg_iter( --argc, ++argv );
  CmdLine cmd(*argv, &partition_name, &events, &datafilename,
              &multiFileRun, &processData, &noRawOutput,
              &skipEvents, &startFile, NULL);
  cmd.description( "RCD DAQ event dumper" );
  cmd.parse( arg_iter );
   
  try {
    IPCCore::init( argc, argv );
  } catch( daq::ipc::Exception & ex ) {
    ers::fatal( ex );
  }

  signal(SIGTERM, term_handler);
  IPCPartition partition(partition_name);

  EventReaderBase *reader = 0;
  try{
    if (datafilename.isNULL()) {
      reader = new DAQEventReader(partition, (bool)multiFileRun);
      
    }
    else {
      reader = new FileEventReader(string(datafilename),
                        (bool)multiFileRun, (int)events, (int)skipEvents,
                        (int)startFile);
    }
    ProcessEvents(*reader, events, processData, noRawOutput);
  }catch (exception &e){
    cout << e.what() << endl;
  }

  return 0;
}

void
ProcessEvents(EventReaderBase &evReader, int nEvents, bool processData,
              bool noRawOutput)
{
  const int eventsInFile = evReader.GetNumberOfEvents();
  cout << "Number of events requested: "
       << ((nEvents==-1) ? "all" : to_string(nEvents))
       << endl;

  int eventCount = 0;
  vector<unsigned int> rawEvent;
  while ( ! (rawEvent = evReader.GetNextRawEvent()).empty() ) {

      cout << "\n# Event No.: " << eventCount << " size: "
           << rawEvent.size()
           << " L1ID: " << RCDRawEvent::GetEventNumber(rawEvent)
           << endl; 
      if (! noRawOutput){
        cout << "Raw data:";
        int chunk=0;
        for(const unsigned int& it : rawEvent) {
          if (chunk++ % 4 == 0){
            cout << "\n  ";
          }
          cout << boost::format("%08x") % it << " ";
        }
        cout << endl;
      }

      if(processData){
        RCDRawEvent rcdEvent(rawEvent);
        cout << "RCDRawEvent:\n* Valid: "
             << ((rcdEvent.isValid()) ? "true" : "false")
             << endl;
        if (rcdEvent.isValid()){
          cout << "  QDC data streams: " << rcdEvent.v792_data.size() << endl;   // QDC
          cout << "  TDC data streams: " << rcdEvent.v1290_data.size() << endl;  // TDC
          cout << "  Scaler data streams: " << rcdEvent.v560_data.size() << endl;   // Scaler
          cout << "  Micromegas data: " << ((rawEvent[9] == 0x0a000002)  ? "Found" : "N/A") << endl;
        }
      }
      ++eventCount;
  }
  cout << "\n" << endl;
}

void 
term_handler(int param) {
  assert(param == 15);
  std::cout << "Handling SIGTERM... exiting" << std::endl;
  exit(0);
}
