#!/bin/bash

ROOT=$(cd `dirname $0`; pwd -P)

if [ -z "$TDAQ_PARTITION" ]; then
  source setup_RCDTDAQ.sh
fi

ohp --partition $TDAQ_PARTITION --config $ROOT/Configs/bl4s_2016_ohp.xml
